// @file gpio_isr.c
#include "gpio_isr.h"
#include "lpc40xx.h"
#include "stdbool.h"

static function_pointer_t gpio0_callbacks_rising[32];
static function_pointer_t gpio0_callbacks_falling[32];

static function_pointer_t gpio2_callbacks_rising[32];
static function_pointer_t gpio2_callbacks_falling[32];

void gpio0__attach_interrupt(uint32_t port, uint32_t pin, gpio_interrupt_e interrupt_type,
                             function_pointer_t callback) {
  if (port == 0) {
    if (interrupt_type == GPIO_INTR__RISING_EDGE) {
      LPC_GPIOINT->IO0IntEnR |= (1 << pin);
      gpio0_callbacks_rising[pin] = callback;
    } else if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
      LPC_GPIOINT->IO0IntEnF |= (1 << pin);
      gpio0_callbacks_falling[pin] = callback;
    }
  } else if (port == 2) {
    if (interrupt_type == GPIO_INTR__RISING_EDGE) {
      LPC_GPIOINT->IO2IntEnR |= (1 << pin);
      gpio2_callbacks_rising[pin] = callback;
    } else if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
      LPC_GPIOINT->IO2IntEnF |= (1 << pin);
      gpio2_callbacks_falling[pin] = callback;
    }
  }
}

static void find_port_pin_that_generated_interrupt(uint32_t *port, uint32_t *pin) {
  const uint32_t port0_interrupt_status_bit = 1;
  uint32_t rising_status_register_bits;
  uint32_t falling_status_register_bits;

  // IntStatus ----> bit0 = P0INT and bit2 = P2INT
  // Check Port 0 first, if no interrupts on Port0 then Port2 has an interrupt
  *port = (LPC_GPIOINT->IntStatus & port0_interrupt_status_bit) ? GPIO_PORT0 : GPIO_PORT2;

  rising_status_register_bits = (*port == GPIO_PORT0) ? LPC_GPIOINT->IO0IntStatR : LPC_GPIOINT->IO2IntStatR;
  falling_status_register_bits = (*port == GPIO_PORT0) ? LPC_GPIOINT->IO0IntStatF : LPC_GPIOINT->IO2IntStatF;

  const uint32_t max_pin_nums = 32;
  for (uint32_t pin_num = 0; pin_num < max_pin_nums; pin_num++) {
    if ((rising_status_register_bits & (1 << pin_num)) || (falling_status_register_bits & (1 << pin_num))) {
      *pin = pin_num;
      break;
    }
  }
}

static function_pointer_t get_interrupt_callback_ptr(uint32_t port, uint32_t pin) {
  function_pointer_t attached_user_handler = 0;
  uint32_t rising_status_register_bits;
  uint32_t falling_status_register_bits;

  rising_status_register_bits = (port == GPIO_PORT0) ? LPC_GPIOINT->IO0IntStatR : LPC_GPIOINT->IO2IntStatR;
  falling_status_register_bits = (port == GPIO_PORT0) ? LPC_GPIOINT->IO0IntStatF : LPC_GPIOINT->IO2IntStatF;

  switch (port) {
  case GPIO_PORT0:
    if (rising_status_register_bits & (1 << pin)) {
      attached_user_handler = gpio0_callbacks_rising[pin];
    } else if (falling_status_register_bits & (1 << pin)) {
      attached_user_handler = gpio0_callbacks_falling[pin];
    }
    break;
  case GPIO_PORT2:
    if (rising_status_register_bits & (1 << pin)) {
      attached_user_handler = gpio2_callbacks_rising[pin];
    } else if (falling_status_register_bits & (1 << pin)) {
      attached_user_handler = gpio2_callbacks_falling[pin];
    }
    break;
  }

  return attached_user_handler;
}

void clear_port_pin_interrupt(uint32_t port, uint32_t pin) {
  if (port == GPIO_PORT0) {
    LPC_GPIOINT->IO0IntClr |= (1 << pin);
  } else if (port == GPIO_PORT2) {
    LPC_GPIOINT->IO2IntClr |= (1 << pin);
  }
}

void gpio0__interrupt_dispatcher(void) {
  uint32_t port_that_generated_interrupt;
  uint32_t pin_that_generated_interrupt;
  function_pointer_t attached_user_handler;

  find_port_pin_that_generated_interrupt(&port_that_generated_interrupt, &pin_that_generated_interrupt);
  attached_user_handler = get_interrupt_callback_ptr(port_that_generated_interrupt, pin_that_generated_interrupt);

  // Invoke the user registered callback, and then clear the interrupt
  attached_user_handler();
  clear_port_pin_interrupt(port_that_generated_interrupt, pin_that_generated_interrupt);
}