#pragma once
#include "stdint.h"
uint8_t countdown__clear_number[32][32];

uint8_t countdown__3_block_size_1[8][5];
uint8_t countdown__3_block_size_2[16][10];
uint8_t countdown__3_block_size_3[24][15];
uint8_t countdown__3_block_size_4[32][20];

uint8_t countdown__2_block_size_1[8][5];
uint8_t countdown__2_block_size_2[16][10];
uint8_t countdown__2_block_size_3[24][15];
uint8_t countdown__2_block_size_4[32][20];

uint8_t countdown__1_block_size_1[8][3];
uint8_t countdown__1_block_size_2[16][6];
uint8_t countdown__1_block_size_3[24][9];
uint8_t countdown__1_block_size_4[32][12];

uint8_t countdown__go_block_size_1[8][13];
uint8_t countdown__go_block_size_2[16][26];
uint8_t countdown__go_block_size_3[24][39];
uint8_t countdown__go_block_size_4[32][52];

uint8_t countdown__go_clear_for_game[32][52];