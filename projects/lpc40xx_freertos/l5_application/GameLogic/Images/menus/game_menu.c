#include "game_menu.h"

// clang-format off





uint8_t game_menu__one_player[10][28] = {
{'z','c','z','z','z','c','c','c','z','z','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'c','c','z','z','z','c','z','z','c','z','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','c','z','z','z','c','z','z','c','z','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','c','z','z','z','c','c','c','z','z','c','z','z','z','z','z','z','z','z','z','z','z','c','z','z','z','c','z'},
{'z','c','z','z','z','c','z','z','z','z','c','z','z','c','z','z','z','c','z','c','z','c','z','c','z','c','z','c'},
{'z','c','z','z','z','c','z','z','z','z','c','z','c','z','c','z','z','c','z','c','z','c','c','c','z','c','z','z'},
{'z','c','z','z','z','c','z','z','z','z','c','z','c','z','c','z','z','c','c','c','z','c','z','z','z','c','z','z'},
{'c','c','c','z','z','c','z','z','z','z','c','z','z','c','c','c','z','z','z','c','z','z','c','c','z','c','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','c','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','c','z','z','z','z','z','z','z','z','z'}};																											

uint8_t game_menu__two_player[10][28] = {
{'z','c','c','z','z','c','c','c','z','z','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'c','z','z','c','z','c','z','z','c','z','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','c','z','c','z','z','c','z','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','c','z','c','c','c','z','z','c','z','z','z','z','z','z','z','z','z','z','z','c','z','z','z','c','z'},
{'z','z','c','z','z','c','z','z','z','z','c','z','z','c','z','z','z','c','z','c','z','c','z','c','z','c','z','c'},
{'z','c','z','z','z','c','z','z','z','z','c','z','c','z','c','z','z','c','z','c','z','c','c','c','z','c','z','z'},
{'c','z','z','z','z','c','z','z','z','z','c','z','c','z','c','z','z','c','c','c','z','c','z','z','z','c','z','z'},
{'c','c','c','c','z','c','z','z','z','z','c','z','z','c','c','c','z','z','z','c','z','z','c','c','z','c','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','c','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','c','z','z','z','z','z','z','z','z','z'}};	

uint8_t game_menu__classic[10][28] = {
{'z','z','c','c','c','z','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','c','z','z','z','z','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','c','z','z','z','z','c','z','c','c','z','z','z','c','c','c','z','c','c','c','z','c','z','z','c','c','z','z'},
{'z','c','z','z','z','z','c','z','c','z','c','z','z','c','z','z','z','c','z','z','z','c','z','c','z','z','z','z'},
{'z','c','z','z','z','z','c','z','c','z','c','z','z','c','c','c','z','c','c','c','z','c','z','c','z','z','z','z'},
{'z','c','z','z','z','z','c','z','c','z','c','z','z','z','z','c','z','z','z','c','z','c','z','c','z','z','z','z'},
{'z','z','c','c','c','z','c','z','c','c','c','c','z','c','c','c','z','c','c','c','z','c','z','z','c','c','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'}};	

uint8_t game_menu__turbo[10][28] = {
{'z','z','z','c','c','c','c','c','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','c','z','z','z','z','z','z','z','z','z','z','z','c','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','c','z','z','z','z','z','z','z','z','z','z','z','c','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','c','z','z','c','z','z','c','z','z','c','z','z','c','c','z','z','z','c','c','z','z','z','z'},
{'z','z','z','z','z','c','z','z','c','z','z','c','z','c','z','c','z','c','z','c','z','c','z','c','z','z','z','z'},
{'z','z','z','z','z','c','z','z','c','z','z','c','z','c','z','z','z','c','z','c','z','c','z','c','z','z','z','z'},
{'z','z','z','z','z','c','z','z','z','c','c','z','z','c','z','z','z','c','c','z','z','c','c','z','z','z','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'},
{'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'}};

// clang-format on

uint8_t game_menu__space[10][28] = {{'z', 'p', 'p', 'p', 'z', 'z', 'z', 'p', 'p', 'z', 'z', 'z', 'p', 'p',
                                     'z', 'z', 'z', 'p', 'p', 'p', 'z', 'z', 'z', 'p', 'p', 'p', 'z', 'z'},
                                    {'p', 'z', 'z', 'z', 'z', 'z', 'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z',
                                     'p', 'z', 'p', 'z', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'z', 'z', 'z'},
                                    {'p', 'z', 'z', 'z', 'z', 'z', 'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z',
                                     'p', 'z', 'p', 'z', 'z', 'z', 'z', 'z', 'p', 'z', 'z', 'z', 'z', 'z'},
                                    {'p', 'p', 'p', 'p', 'z', 'z', 'p', 'p', 'p', 'z', 'z', 'p', 'p', 'p',
                                     'p', 'z', 'p', 'z', 'z', 'z', 'z', 'z', 'p', 'p', 'p', 'z', 'z', 'z'},
                                    {'z', 'z', 'z', 'p', 'z', 'z', 'p', 'z', 'z', 'z', 'z', 'p', 'z', 'z',
                                     'p', 'z', 'p', 'z', 'z', 'z', 'z', 'z', 'p', 'z', 'z', 'z', 'z', 'z'},
                                    {'z', 'z', 'z', 'p', 'z', 'z', 'p', 'z', 'z', 'z', 'z', 'p', 'z', 'z',
                                     'p', 'z', 'p', 'z', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'z', 'z', 'z'},
                                    {'p', 'p', 'p', 'z', 'z', 'z', 'p', 'z', 'z', 'z', 'z', 'p', 'z', 'z',
                                     'p', 'z', 'z', 'p', 'p', 'p', 'z', 'z', 'z', 'p', 'p', 'p', 'z', 'z'},
                                    {'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z',
                                     'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z'},
                                    {'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z',
                                     'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z'},
                                    {'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z',
                                     'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z', 'z'}};

uint8_t game_menu__rage[7][21] = {
    {'p', 'p', 'p', 'z', 'z', 'z', 'p', 'p', 'z', 'z', 'z', 'p', 'p', 'p', 'z', 'z', 'z', 'p', 'p', 'p', 'z'},
    {'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'z', 'z', 'z', 'p', 'z', 'z', 'z', 'p'},
    {'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'z', 'z', 'z', 'p', 'z', 'z', 'z', 'z'},
    {'p', 'p', 'p', 'z', 'z', 'p', 'p', 'p', 'p', 'z', 'p', 'z', 'p', 'p', 'z', 'z', 'p', 'p', 'p', 'z', 'z'},
    {'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'z', 'z'},
    {'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'z', 'p'},
    {'p', 'z', 'z', 'p', 'z', 'p', 'z', 'z', 'p', 'z', 'z', 'p', 'p', 'p', 'z', 'z', 'z', 'p', 'p', 'p', 'z'}};