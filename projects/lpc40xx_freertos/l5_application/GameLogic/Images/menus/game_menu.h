#pragma once
#include <stdint.h>

// clang-format off





extern uint8_t game_menu__one_player[10][28];																									

extern uint8_t game_menu__two_player[10][28];

extern uint8_t game_menu__classic[10][28] ;
extern uint8_t game_menu__turbo[10][28] ;

uint8_t game_menu__space[10][28] ;
uint8_t game_menu__rage[7][21] ;
// clang-format on
