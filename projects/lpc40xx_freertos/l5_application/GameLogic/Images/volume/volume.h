#pragma once
#include <stdint.h>

extern uint8_t volume__level_0[10][34];
extern uint8_t volume__level_1[10][34];
extern uint8_t volume__level_2[10][34];
extern uint8_t volume__level_3[10][34];
extern uint8_t volume__level_4[10][34];
extern uint8_t volume__level_5[10][34];
extern uint8_t volume__level_6[10][34];
extern uint8_t volume__level_7[10][34];
extern uint8_t volume__level_8[10][34];

extern uint8_t volume__clear_indicator[10][34];