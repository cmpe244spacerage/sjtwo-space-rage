#pragma once

#include <stdbool.h>

typedef enum {
  space_rage,
  title_screen,
  classic_mode,
  turbo_mode,
  game_music,
  count_sequence,
  lives_music,
  p1_menu_cursor,
  p1_game_movement,
  p1_explosion,
  p1_wins,
  p1_lose_life,
  p2_menu_cursor,
  p2_game_movement,
  p2_explosion,
  p2_wins,
  p2_lose_life,
  versus_mode,
  single_player,
  game_over,
  draw,
  you_win,
  you_lose,
  game_over_music,
  volume_0,
  volume_1,
  volume_2,
  volume_3,
  volume_4,
  volume_5,
  volume_6,
  volume_7,
  volume_8,
  stop_music,
} mp3_cmd;

void mp3__init(void);
void mp3__uart3_send_cmd(mp3_cmd cmd);
bool mp3__check_volume_buttons(void);
void mp3__clear_volume_indicator(void);
