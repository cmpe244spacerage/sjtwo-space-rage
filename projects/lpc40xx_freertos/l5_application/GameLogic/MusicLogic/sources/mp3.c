#include <stdint.h>

#include "mp3.h"

#include "FreeRTOS.h"
#include "board_io.h"
#include "clock.h"
#include "gpio.h"
#include "queue.h"
#include "uart.h"

#include "game_board.h"
#include "gpio_isr.h"
#include "semphr.h"
#include "volume.h"

static void mp3__uart3_init(void);
static void mp3__volume_init(void);

static void volume_isr(void);

static QueueHandle_t rx_queue;
static QueueHandle_t tx_queue;

static SemaphoreHandle_t volume_change_semphr;

const static gpio_s volume_sensor = {.port_number = 0, .pin_number = 0};

void mp3__init(void) {
  mp3__uart3_init();
  mp3__volume_init();
}
void mp3__uart3_send_cmd(mp3_cmd cmd) { uart__put(UART__3, cmd, 0); }
bool mp3__check_volume_buttons(void) {
  bool volume_changed = false;
  static const mp3_cmd MAX_VOLUME_LEVEL = volume_8;
  static mp3_cmd VOLUME_LEVEL = volume_4;
  const location target_location = {.x_pos = 31, .y_pos = 58};
  //
  if (gpio__get(volume_sensor)) {
    //  if (xSemaphoreTake(volume_change_semphr, 0)) {
    volume_changed = true;
    if (VOLUME_LEVEL == MAX_VOLUME_LEVEL) {
      VOLUME_LEVEL = volume_0;
    } else {
      VOLUME_LEVEL++;
    }

    switch (VOLUME_LEVEL) {
    case volume_0:
      game_board__draw_image_center_target(10, 34, volume__level_0, target_location);
      mp3__uart3_send_cmd(volume_0);
      break;
    case volume_1:
      game_board__draw_image_center_target(10, 34, volume__level_1, target_location);
      mp3__uart3_send_cmd(volume_1);
      break;
    case volume_2:
      game_board__draw_image_center_target(10, 34, volume__level_2, target_location);
      mp3__uart3_send_cmd(volume_2);
      break;
    case volume_3:
      game_board__draw_image_center_target(10, 34, volume__level_3, target_location);
      mp3__uart3_send_cmd(volume_3);
      break;
    case volume_4:
      game_board__draw_image_center_target(10, 34, volume__level_4, target_location);
      mp3__uart3_send_cmd(volume_4);
      break;
    case volume_5:
      game_board__draw_image_center_target(10, 34, volume__level_5, target_location);
      mp3__uart3_send_cmd(volume_5);
      break;
    case volume_6:
      game_board__draw_image_center_target(10, 34, volume__level_6, target_location);
      mp3__uart3_send_cmd(volume_6);
      break;
    case volume_7:
      game_board__draw_image_center_target(10, 34, volume__level_7, target_location);
      mp3__uart3_send_cmd(volume_7);
      break;
    case volume_8:
      game_board__draw_image_center_target(10, 34, volume__level_8, target_location);
      mp3__uart3_send_cmd(volume_8);
      break;
    default:
      break;
    }
  }
  return volume_changed;
}
void mp3__clear_volume_indicator(void) {
  const location target_location = {.x_pos = 31, .y_pos = 58};
  game_board__draw_image_center_target(10, 34, volume__clear_indicator, target_location);
}
/* Private Functions */
static void mp3__uart3_init(void) {
  /* Initialize pins for UART3 */
  const gpio_s uart_tx_pin = {.port_number = 4, .pin_number = 28};
  const gpio_s uart_rx_pin = {.port_number = 4, .pin_number = 29};
  const gpio__function_e uart_function = GPIO__FUNCTION_2;

  gpio__set_function(uart_tx_pin, uart_function);
  gpio__set_function(uart_rx_pin, uart_function);

  /* Enable UART3 and UART Queues */
  const uart_e mp3_uart = UART__3;
  const uint32_t baud_rate = 115200;
  const uint32_t queue_size = 32;

  uart__init(UART__3, clock__get_peripheral_clock_hz(), baud_rate);

  rx_queue = xQueueCreate(queue_size, sizeof(char));
  tx_queue = xQueueCreate(queue_size, sizeof(char));

  uart__enable_queues(mp3_uart, rx_queue, tx_queue);
}

static void mp3__volume_init(void) {
  gpio__set_function(volume_sensor, GPIO__FUNCITON_0_IO_PIN);

  gpio__set_as_input(volume_sensor);

  gpio__enable_pull_down_resistors(volume_sensor);

  //  gpio0__attach_interrupt(volume_sensor.port_number, volume_sensor.pin_number, GPIO_INTR__RISING_EDGE, volume_isr);
  //  volume_change_semphr = xSemaphoreCreateBinary();
}

static void volume_isr(void) { xSemaphoreGiveFromISR(volume_change_semphr, NULL); }
