#pragma once
#include "game_board.h"
#include "game_structs.h"
#include "player.h"

void ai_base__check_to_not_die(player_s *ai_player, bool *valid_ways_to_move);
void ai_base__easy_dont_get_trapped(player_s *ai_player, bool *valid_ways_to_move);
current_direction_e ai_base__normal_look_ahead_more(player_s *ai_player, bool *valid_ways_to_move, uint8_t min_range,
                                                    uint8_t max_range);
void ai_base__hard_cut_in_half(player_s *ai_player, bool *valid_ways_to_move, location opponent_location);
void ai_base__hard_segregate_player(player_s *ai_player, bool *valid_ways_to_move, location opponent_location);
void ai_base__hard_move_to_other_section_away_from_other_player(player_s *ai_player, bool *valid_ways_to_move,
                                                                location opponent_location);

void ai_base__ai_decide(player_s *ai_player, location opponent_location);
void ai_base__easy_ai_decide(player_s *ai_player);
void ai_base__normal_ai_decide(player_s *ai_player);
void ai_base__hard_ai_decide(player_s *ai_player, location opponent_location);