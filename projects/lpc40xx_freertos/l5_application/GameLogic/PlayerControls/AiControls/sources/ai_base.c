#include "ai_base.h"

void ai_base__check_to_not_die(player_s *ai_player, bool *valid_ways_to_move) {
  location current_location = ai_player->player_details.current_location;
  current_direction_e current_direction = ai_player->player_details.drawn_direction;
  uint8_t cells_clear_in_direction[4] = {0, 0, 0, 0};

  // check 2x3 cells all around ship
  /* x = ship

        . . .
        . . .
    . . . x . . .
    . . x . x . .
        . . .
        . . .
  */
  // north
  for (int i = -3; i < -1; i++) {
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION, i))) {
      cells_clear_in_direction[0] += 1;
    }
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, 0, i))) {
      cells_clear_in_direction[0] += 1;
    }
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, EAST_DIRECTION, i))) {
      cells_clear_in_direction[0] += 1;
    }
  }
  // EAST
  for (int i = 2; i < 4; i++) {
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, i, NORTH_DIRECTION))) {
      cells_clear_in_direction[1] += 1;
    }
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, i, 0))) {
      cells_clear_in_direction[1] += 1;
    }
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, i, SOUTH_DIRECTION))) {
      cells_clear_in_direction[1] += 1;
    }
  }
  // SOUTH
  for (int i = 2; i < 4; i++) {
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION, i))) {
      cells_clear_in_direction[2] += 1;
    }
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, 0, i))) {
      cells_clear_in_direction[2] += 1;
    }
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION, i))) {
      cells_clear_in_direction[2] += 1;
    }
  }

  // WEST
  for (int i = -3; i < -1; i++) {
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, i, NORTH_DIRECTION))) {
      cells_clear_in_direction[3] += 1;
    }
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, i, 0))) {
      cells_clear_in_direction[3] += 1;
    }
    if (!game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, i, SOUTH_DIRECTION))) {
      cells_clear_in_direction[3] += 1;
    }
  }

  // 3 out of 6 need to be free
  if (cells_clear_in_direction[current_direction] >= 3) {
    valid_ways_to_move[current_direction] =
        true & !game_board__check_forward_for_player(current_location, current_direction);
  }
  // 6 of 6 cells need to be free
  if (cells_clear_in_direction[(current_direction + 1) % 4] >= 6) {
    valid_ways_to_move[(current_direction + 1) % 4] = true;
  }
  if (cells_clear_in_direction[(current_direction + 3) % 4] >= 6) {
    valid_ways_to_move[(current_direction + 3) % 4] = true;
  }

  // Can't choose backward should be taken care of by prior logic, but explicit here
  valid_ways_to_move[(current_direction + 2) % 4] = false;

  if (current_direction == NORTH || current_direction == SOUTH) {
    // check one cell back and left/right
    int y_check = (current_direction == NORTH) ? SOUTH_DIRECTION : NORTH_DIRECTION;
    if (game_board__check_cell(
            game_structs__add_location_offsets_by_row_col(current_location, EAST_DIRECTION * 2, y_check))) {
      valid_ways_to_move[EAST] = false;
    }
    if (game_board__check_cell(
            game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION * 2, y_check))) {
      valid_ways_to_move[WEST] = false;
    }
  } else {
    int x_check = (current_direction == EAST) ? WEST_DIRECTION : EAST_DIRECTION;
    if (game_board__check_cell(
            game_structs__add_location_offsets_by_row_col(current_location, x_check, NORTH_DIRECTION * 2))) {
      valid_ways_to_move[NORTH] = false;
    }
    if (game_board__check_cell(
            game_structs__add_location_offsets_by_row_col(current_location, x_check, SOUTH_DIRECTION * 2))) {
      valid_ways_to_move[SOUTH] = false;
    }
  }
}
void ai_base__easy_dont_get_trapped(player_s *ai_player, bool *valid_ways_to_move) {
  location current_location = ai_player->player_details.current_location;
  current_direction_e current_direction = ai_player->player_details.drawn_direction;

  if (current_direction == NORTH || current_direction == SOUTH) {
    bool east_still_open = true;
    bool west_still_open = true;
    int y_offset = (current_direction == NORTH) ? -2 : 2;
    for (int i = 2; i <= 4; i++) {
      if (game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, i, y_offset))) {
        east_still_open = false;
      }
    }
    for (int i = -4; i <= -2; i++) {
      if (game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, i, y_offset))) {
        west_still_open = false;
      }
    }
    if (west_still_open == false && east_still_open == false) {
      valid_ways_to_move[current_direction] = false;
    }
  } else {
    bool north_still_open = true;
    bool south_still_open = true;
    int x_offset = (current_direction == WEST) ? -2 : 2;
    for (int i = 2; i <= 4; i++) {
      if (game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, x_offset, i))) {
        south_still_open = false;
      }
    }
    for (int i = -4; i <= -2; i++) {
      if (game_board__check_cell(game_structs__add_location_offsets_by_row_col(current_location, x_offset, i))) {
        north_still_open = false;
      }
    }
    if (north_still_open == false && south_still_open == false) {
      valid_ways_to_move[current_direction] = false;
    }
  }
}

current_direction_e ai_base__normal_look_ahead_more(player_s *ai_player, bool *valid_ways_to_move, uint8_t min_range,
                                                    uint8_t max_range) {
  current_direction_e suggest_turn_direction = NORTH;
  current_direction_e current_direction = ai_player->player_details.drawn_direction;
  location current_location = ai_player->player_details.current_location;

  int east_turn_counter = 0;
  int west_turn_counter = 0;
  int north_turn_counter = 0;
  int south_turn_counter = 0;
  for (int i = min_range; i < max_range; i++) {
    if (current_direction == NORTH || current_direction == SOUTH) {
      if (!game_board__check_cell(
              game_structs__add_location_offsets_by_row_col(current_location, EAST_DIRECTION * i, NORTH_DIRECTION))) {
        east_turn_counter++;
      }
      if (!game_board__check_cell(
              game_structs__add_location_offsets_by_row_col(current_location, EAST_DIRECTION * i, 0))) {
        east_turn_counter++;
      }
      if (!game_board__check_cell(
              game_structs__add_location_offsets_by_row_col(current_location, EAST_DIRECTION * i, SOUTH_DIRECTION))) {
        east_turn_counter++;
      }
      if (!game_board__check_cell(
              game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION * i, NORTH_DIRECTION))) {
        west_turn_counter++;
      }
      if (!game_board__check_cell(
              game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION * i, 0))) {
        west_turn_counter++;
      }
      if (!game_board__check_cell(
              game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION * i, SOUTH_DIRECTION))) {
        west_turn_counter++;
      }
    } else {
      if (current_direction == EAST || current_direction == WEST) {
        if (!game_board__check_cell(
                game_structs__add_location_offsets_by_row_col(current_location, EAST_DIRECTION, NORTH_DIRECTION * i))) {
          north_turn_counter++;
        }
        if (!game_board__check_cell(
                game_structs__add_location_offsets_by_row_col(current_location, 0, NORTH_DIRECTION * i))) {
          north_turn_counter++;
        }
        if (!game_board__check_cell(
                game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION, NORTH_DIRECTION * i))) {
          north_turn_counter++;
        }
        if (!game_board__check_cell(
                game_structs__add_location_offsets_by_row_col(current_location, WEST_DIRECTION, SOUTH_DIRECTION * i))) {
          south_turn_counter++;
        }
        if (!game_board__check_cell(
                game_structs__add_location_offsets_by_row_col(current_location, 0, SOUTH_DIRECTION * i))) {
          south_turn_counter++;
        }
        if (!game_board__check_cell(
                game_structs__add_location_offsets_by_row_col(current_location, EAST_DIRECTION, SOUTH_DIRECTION * i))) {
          south_turn_counter++;
        }
      }
    }
  }
  if (current_direction == NORTH || current_direction == SOUTH) {
    if (east_turn_counter > west_turn_counter) {
      suggest_turn_direction = EAST;
    } else {
      suggest_turn_direction = WEST;
    }
  } else {
    if (south_turn_counter > north_turn_counter) {
      suggest_turn_direction = SOUTH;
    } else {
      suggest_turn_direction = NORTH;
    }
  }
  return suggest_turn_direction;
}
void ai_base__hard_cut_in_half(player_s *ai_player, bool *valid_ways_to_move, location opponent_location) {

  current_direction_e current_direction = ai_player->player_details.drawn_direction;
  location current_location = ai_player->player_details.current_location;

  uint8_t x_target = 59;
  uint8_t y_target = 59;
  int8_t opponent_difference_y = opponent_location.y_pos - current_location.y_pos + 25;
  int8_t opponent_difference_x = opponent_location.x_pos - current_location.x_pos + 25;
  if (valid_ways_to_move[SOUTH] && valid_ways_to_move[EAST] && ai_player->player_details.ai_logic_state == 0 &&
      (opponent_difference_x > 0 && opponent_difference_y > 0)) {
    if (x_target - current_location.x_pos < y_target - current_location.y_pos) {
      ai_player->player_details.new_direction = SOUTH;
    } else {
      ai_player->player_details.new_direction = EAST;
    }
  } else {
    ai_player->player_details.ai_logic_state = 2;
  }
}

void ai_base__hard_segregate_player(player_s *ai_player, bool *valid_ways_to_move, location opponent_location) {

  location current_location = ai_player->player_details.current_location;
  int8_t opponent_difference_y = opponent_location.y_pos - current_location.y_pos;
  int8_t opponent_difference_x = opponent_location.x_pos - current_location.x_pos;

  if (valid_ways_to_move[SOUTH] && valid_ways_to_move[EAST] && ai_player->player_details.ai_logic_state == 1) {
    if (opponent_difference_y > opponent_difference_x) {
      ai_player->player_details.new_direction = EAST;
    } else {
      ai_player->player_details.new_direction = SOUTH;
    }
  } else {
    ai_player->player_details.ai_logic_state = 2;
  }
}

void ai_base__hard_move_to_other_section_away_from_other_player(player_s *ai_player, bool *valid_ways_to_move,
                                                                location opponent_location) {
  current_direction_e current_direction = ai_player->player_details.drawn_direction;

  if (current_direction == SOUTH) {
    ai_player->player_details.new_direction = EAST;
  } else {
    ai_player->player_details.new_direction = SOUTH;
  }
  ai_player->player_details.ai_logic_state = 3;
}

void ai_base__ai_decide(player_s *ai_player, location opponent_location) {
  if (ai_player->player_details.ticks_till_move == 1) {
    switch (ai_player->ai_difficulty) {
    case EASY:
      ai_base__easy_ai_decide(ai_player);
      break;
    case NORMAL:
      ai_base__normal_ai_decide(ai_player);
      break;
    case HARD:
      ai_base__hard_ai_decide(ai_player, opponent_location);
    default:
      break;
    }
  }
}

void ai_base__easy_ai_decide(player_s *ai_player) {

  current_direction_e turn_direction = ai_player->player_details.drawn_direction;
  // location cell_to_trap_test = {8, 32};
  // game_board__draw_cell(cell_to_trap_test, PURPLE);
  bool valid_ways_to_move[4] = {false, false, false, false};
  ai_base__check_to_not_die(ai_player, valid_ways_to_move);
  ai_base__easy_dont_get_trapped(ai_player, valid_ways_to_move);
  current_direction_e suggest_direction = ai_base__normal_look_ahead_more(ai_player, valid_ways_to_move, 4, 30);

  // if current direction is no loger valid
  if (valid_ways_to_move[turn_direction] == false) {

    if (valid_ways_to_move[(turn_direction + 1) % 4] == true) {
      // if the other direction is better
      if (valid_ways_to_move[(turn_direction + 3) % 4] == true && suggest_direction == (turn_direction + 3) % 4) {
        turn_direction = (turn_direction + 3) % 4;
      }
      // this direction is better
      else {
        turn_direction = (turn_direction + 1) % 4;
      }
    }
    // other direction was invalid
    else if (valid_ways_to_move[(turn_direction + 3) % 4] == true) {
      turn_direction = (turn_direction + 3) % 4;
    }
  }

  ai_player->player_details.new_direction = turn_direction;
}

void ai_base__normal_ai_decide(player_s *ai_player) {
  current_direction_e turn_direction = ai_player->player_details.drawn_direction;
  // location ai_location = {31, 53};
  // location north_decide = {31, 52};
  // location east_decide = {32, 53};
  // location west_decide = {30, 53};
  // location south_decide = {31, 54};

  // location ai_valid_location = {34, 53};
  // location north_valid_decide = {34, 52};
  // location east_valid_decide = {35, 53};
  // location west_valid_decide = {33, 53};
  // location south_valid_decide = {34, 54};
  // game_board__draw_cell(ai_valid_location, YELLOW);
  // game_board__clear_cell(north_valid_decide);
  // game_board__clear_cell(east_valid_decide);
  // game_board__clear_cell(south_valid_decide);
  // game_board__clear_cell(west_valid_decide);

  // game_board__draw_cell(ai_location, PURPLE);
  // game_board__clear_cell(north_decide);
  // game_board__clear_cell(east_decide);
  // game_board__clear_cell(south_decide);
  // game_board__clear_cell(west_decide);

  bool valid_ways_to_move[4] = {false, false, false, false};
  ai_base__check_to_not_die(ai_player, valid_ways_to_move);

  // if (valid_ways_to_move[NORTH] == true) {
  //   game_board__draw_cell(north_valid_decide, CYAN);
  // }
  // if (valid_ways_to_move[EAST] == true) {
  //   game_board__draw_cell(east_valid_decide, CYAN);
  // }

  // if (valid_ways_to_move[SOUTH] == true) {
  //   game_board__draw_cell(south_valid_decide, CYAN);
  // }

  // if (valid_ways_to_move[WEST] == true) {
  //   game_board__draw_cell(west_valid_decide, CYAN);
  // }

  ai_base__easy_dont_get_trapped(ai_player, valid_ways_to_move);
  current_direction_e suggest_direction = ai_base__normal_look_ahead_more(ai_player, valid_ways_to_move, 4, 30);

  // if current direction is no loger valid
  if (valid_ways_to_move[turn_direction] == false) {

    if (valid_ways_to_move[(turn_direction + 1) % 4] == true) {
      // if the other direction is better
      if (valid_ways_to_move[(turn_direction + 3) % 4] == true && suggest_direction == (turn_direction + 3) % 4) {
        turn_direction = (turn_direction + 3) % 4;
      }
      // this direction is better
      else {
        turn_direction = (turn_direction + 1) % 4;
      }

    }
    // other direction was invalid
    else if (valid_ways_to_move[(turn_direction + 3) % 4] == true) {
      turn_direction = (turn_direction + 3) % 4;
    }
  }
  // Random new direction 50?% of time
  int random_direction = rand() % 100;
  if (random_direction < 3) {

    if (valid_ways_to_move[(turn_direction + 1) % 4] == true && suggest_direction == (turn_direction + 1) % 4) {
      turn_direction = (turn_direction + 1) % 4;

    } else if (valid_ways_to_move[(turn_direction + 3) % 4] == true) {
      turn_direction = (turn_direction + 3) % 4;
    }
  }
  ai_player->player_details.new_direction = turn_direction;
  // switch (ai_player->player_details.new_direction) {
  // case NORTH:
  //   game_board__draw_cell(north_decide, CYAN);
  //   break;
  // case EAST:
  //   game_board__draw_cell(east_decide, CYAN);
  //   break;
  // case SOUTH:
  //   game_board__draw_cell(south_decide, CYAN);
  //   break;
  // case WEST:
  //   game_board__draw_cell(west_decide, CYAN);
  //   break;
  // }
}

void ai_base__hard_ai_decide(player_s *ai_player, location opponent_location) {
  current_direction_e turn_direction = ai_player->player_details.drawn_direction;
  bool valid_ways_to_move[4] = {false, false, false, false};
  ai_base__check_to_not_die(ai_player, valid_ways_to_move);

  location ai_state_0 = {50, 50};
  location ai_state_1 = {51, 50};
  location ai_state_2 = {52, 50};
  location ai_state_3 = {53, 50};
  switch (ai_player->player_details.ai_logic_state) {
  case 0:
    ai_base__hard_cut_in_half(ai_player, valid_ways_to_move, opponent_location);
    // game_board__draw_cell(ai_state_0, YELLOW);
    break;
  case 1:
    ai_base__hard_segregate_player(ai_player, valid_ways_to_move, opponent_location);
    // game_board__draw_cell(ai_state_1, YELLOW);
    break;
  case 2:
    ai_base__hard_move_to_other_section_away_from_other_player(ai_player, valid_ways_to_move, opponent_location);
    // game_board__draw_cell(ai_state_2, YELLOW);
    break;

  case 3:
    // game_board__draw_cell(ai_sstate_3, YELLOW);
    ai_base__easy_dont_get_trapped(ai_player, valid_ways_to_move);
    current_direction_e suggest_direction = ai_base__normal_look_ahead_more(ai_player, valid_ways_to_move, 4, 30);

    // if current direction is no loger valid
    if (valid_ways_to_move[turn_direction] == false) {

      if (valid_ways_to_move[(turn_direction + 1) % 4] == true) {
        // if the other direction is better
        if (valid_ways_to_move[(turn_direction + 3) % 4] == true && suggest_direction == (turn_direction + 3) % 4) {
          turn_direction = (turn_direction + 3) % 4;
        }
        // this direction is better
        else {
          turn_direction = (turn_direction + 1) % 4;
        }
      }
      // other direction was invalid
      else if (valid_ways_to_move[(turn_direction + 3) % 4] == true) {
        turn_direction = (turn_direction + 3) % 4;
      }
    }

    ai_player->player_details.new_direction = turn_direction;
    break;
  }
}