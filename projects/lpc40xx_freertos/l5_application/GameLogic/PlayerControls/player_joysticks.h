#pragma once

#include "player.h"

void player_joysticks__init(void);
bool player_joysticks__check_and_process_players_inputs_if_any(player_e player_num, current_direction_e *input);
void player_joysticks__empty_queue(void);
bool player_joysticks__check_and_process_player_button(player_e player_num);
void player_buttons__empty_semaphores(void);