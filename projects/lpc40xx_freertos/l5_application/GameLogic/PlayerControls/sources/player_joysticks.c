#include "player_joysticks.h"
#include "FreeRTOS.h"
#include "queue.h"

#include "gpio.h"
#include "gpio_isr.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include "semphr.h"

static QueueHandle_t player_one_input_queue;
static QueueHandle_t player_two_input_queue;

static SemaphoreHandle_t player_one_push_button_semaphore;
static SemaphoreHandle_t player_two_push_button_semaphore;

/* Player One Buttons ISR */
static void player1__left_button_isr(void);
static void player1__right_button_isr(void);
static void player1__up_button_isr(void);
static void player1__down_button_isr(void);
static void player1__push_button_isr(void);

/* Player Two Buttons ISR */
static void player2__left_button_isr(void);
static void player2__right_button_isr(void);
static void player2__up_button_isr(void);
static void player2__down_button_isr(void);
static void player2__push_button_isr(void);

void player_joysticks__init(void) {

  player_one_input_queue = xQueueCreate(1, sizeof(current_direction_e));
  player_two_input_queue = xQueueCreate(1, sizeof(current_direction_e));

  player_one_push_button_semaphore = xSemaphoreCreateBinary();
  player_two_push_button_semaphore = xSemaphoreCreateBinary();

  gpio_s p1_left = gpio__construct_with_function(2, 6, 0);         // up
  gpio_s p1_right = gpio__construct_with_function(2, 8, 0);        // down
  gpio_s p1_down = gpio__construct_with_function(0, 16, 0);        // left
  gpio_s p1_up = gpio__construct_with_function(0, 17, 0);          // right
  gpio_s p1_push_button = gpio__construct_with_function(0, 22, 0); // 0.22
  gpio_s p1_button_led = gpio__construct_with_function(2, 4, 0);

  gpio__enable_pull_down_resistors(p1_push_button);

  gpio__set_as_input(p1_left);
  gpio__set_as_input(p1_right);
  gpio__set_as_input(p1_down);
  gpio__set_as_input(p1_up);
  gpio__set_as_input(p1_push_button);
  gpio__set_as_output(p1_button_led);
  gpio__set(p1_button_led);

  gpio0__attach_interrupt(p1_left.port_number, p1_left.pin_number, GPIO_INTR__FALLING_EDGE, player1__left_button_isr);
  gpio0__attach_interrupt(p1_right.port_number, p1_right.pin_number, GPIO_INTR__FALLING_EDGE,
                          player1__right_button_isr);
  gpio0__attach_interrupt(p1_down.port_number, p1_down.pin_number, GPIO_INTR__FALLING_EDGE, player1__down_button_isr);
  gpio0__attach_interrupt(p1_up.port_number, p1_up.pin_number, GPIO_INTR__FALLING_EDGE, player1__up_button_isr);
  gpio0__attach_interrupt(p1_push_button.port_number, p1_push_button.pin_number, GPIO_INTR__RISING_EDGE,
                          player1__push_button_isr);

  gpio_s p2_left = gpio__construct_with_function(2, 7, 0);
  gpio_s p2_right = gpio__construct_with_function(2, 9, 0);
  gpio_s p2_down = gpio__construct_with_function(0, 15, 0);
  gpio_s p2_up = gpio__construct_with_function(0, 18, 0);
  gpio_s p2_push_button = gpio__construct_with_function(0, 1, 0);
  gpio_s p2_button_led = gpio__construct_with_function(2, 5, 0);

  gpio__enable_pull_down_resistors(p2_push_button);

  gpio__set_as_input(p2_left);
  gpio__set_as_input(p2_right);
  gpio__set_as_input(p2_down);
  gpio__set_as_input(p2_up);
  gpio__set_as_input(p2_push_button);
  gpio__set_as_output(p2_button_led);
  gpio__set(p2_button_led);

  gpio0__attach_interrupt(p2_left.port_number, p2_left.pin_number, GPIO_INTR__FALLING_EDGE, player2__left_button_isr);
  gpio0__attach_interrupt(p2_right.port_number, p2_right.pin_number, GPIO_INTR__FALLING_EDGE,
                          player2__right_button_isr);
  gpio0__attach_interrupt(p2_down.port_number, p2_down.pin_number, GPIO_INTR__FALLING_EDGE, player2__down_button_isr);
  gpio0__attach_interrupt(p2_up.port_number, p2_up.pin_number, GPIO_INTR__FALLING_EDGE, player2__up_button_isr);
  gpio0__attach_interrupt(p2_push_button.port_number, p2_push_button.pin_number, GPIO_INTR__RISING_EDGE,
                          player2__push_button_isr);
}

bool player_joysticks__check_and_process_players_inputs_if_any(player_e player_num, current_direction_e *input) {
  bool input_received = false;
  switch (player_num) {
  case PLAYER1:
    if (xQueueReceive(player_one_input_queue, input, 0)) {
      input_received = true;
    }
    break;
  case PLAYER2:
    if (xQueueReceive(player_two_input_queue, input, 0)) {
      input_received = true;
    }
    break;
  }

  return input_received;
}
bool player_joysticks__check_and_process_player_button(player_e player_num) {
  bool input_received = false;
  switch (player_num) {
  case PLAYER1:
    if (xSemaphoreTake(player_one_push_button_semaphore, 0)) {
      input_received = true;
    }
    break;
  case PLAYER2:
    if (xSemaphoreTake(player_two_push_button_semaphore, 0)) {
      input_received = true;
    }
    break;
  }
  return input_received;
}

void player_joysticks__empty_queue(void) {
  uint32_t input;

  xQueueReceive(player_one_input_queue, &input, 100);
  xQueueReceive(player_two_input_queue, &input, 100);
}
void player_buttons__empty_semaphores(void) {
  xSemaphoreTake(player_one_push_button_semaphore, 100);
  xSemaphoreTake(player_two_push_button_semaphore, 100);
}
/* PRIVATE FUNCTIONS */
static void player1__left_button_isr(void) {
  current_direction_e input = WEST;
  xQueueSendFromISR(player_one_input_queue, &input, NULL);
}

static void player1__right_button_isr(void) {
  current_direction_e input = EAST;
  xQueueSendFromISR(player_one_input_queue, &input, NULL);
}

static void player1__up_button_isr(void) {
  current_direction_e input = NORTH;
  xQueueSendFromISR(player_one_input_queue, &input, NULL);
}

static void player1__down_button_isr(void) {
  current_direction_e input = SOUTH;
  xQueueSendFromISR(player_one_input_queue, &input, NULL);
}

static void player1__push_button_isr(void) { xSemaphoreGiveFromISR(player_one_push_button_semaphore, NULL); }
static void player2__push_button_isr(void) { xSemaphoreGiveFromISR(player_two_push_button_semaphore, NULL); }

static void player2__left_button_isr(void) {
  current_direction_e input = WEST;
  xQueueSendFromISR(player_two_input_queue, &input, NULL);
}

static void player2__right_button_isr(void) {
  current_direction_e input = EAST;
  xQueueSendFromISR(player_two_input_queue, &input, NULL);
}

static void player2__up_button_isr(void) {
  current_direction_e input = NORTH;
  xQueueSendFromISR(player_two_input_queue, &input, NULL);
}

static void player2__down_button_isr(void) {
  current_direction_e input = SOUTH;
  xQueueSendFromISR(player_two_input_queue, &input, NULL);
}
