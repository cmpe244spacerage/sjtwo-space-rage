#pragma once
#include <stdint.h>

#include "ai_base.h"
#include "image_includes.h"
#include "player.h"

void game_board__remove_ship(location current_location, current_direction_e drawn_direction,
                             current_direction_e new_direction, pixel_color_e color);
void game_board__draw_ship(location current_location, current_direction_e new_direction, pixel_color_e color);
void game_board__draw_trail_line(location current_location, current_direction_e drawn_direction, pixel_color_e color);

void game_board__draw_cell(location current_location, pixel_color_e color);
void game_board__clear_cell(location current_location); // Possibly change to a location type input

void game_board__clear_cell_block_top_left_corner_target(location current_location, uint8_t cols, uint8_t rows);
void game_board__clear_cell_block_center_target(location current_location, uint8_t cols, uint8_t rows);

void game_display__empty_screen(void);
void game_board__draw_border(pixel_color_e border_color);

bool game_board__check_cell(location current_location);
bool game_board__check_forward_for_player(location current_location, current_direction_e new_direction);

void game_board__draw_image_center_target(uint8_t row_count, uint8_t col_count,
                                          uint8_t image_name[row_count][col_count], location center_pixel);
void game_board__draw_image_top_left_target(uint8_t row_count, uint8_t col_count,
                                            uint8_t image_name[row_count][col_count], location top_left_pixel);
void game_board__draw_screen(uint8_t screen_image[64][64]);

pixel_color_e game_board__translate_abriviation(uint8_t char_in);

// Animations
void game_board__draw_explosion(location target_location);
void game_board__draw_lives_screen(uint8_t player1_lives, uint8_t player2_lives, bool player1_has_crashed,
                                   bool player2_has_crashed);
void game_board__draw_title(void);
void game_board__draw_countdown(void);
void game_board__draw_volume(void);