#pragma once

#include "game_display.h"

typedef struct {
  player_s *player1;
  player_s *player2;
  uint32_t currentTick;
  bool isPaused;
  bool isFinished;
  bool isGameOver;
} game_controller;

typedef enum {
  GAME_TITLE,
  GAME_PLAYER_SELECTION_DRAW,
  GAME_PLAYER_SELECTION,
  GAME_MODE_SELECTION_DRAW,
  GAME_MODE_SELECTION,
  GAME_RESET_LIVES,
  GAME_ROUND_INIT,
  GAME_TURBO,
  GAME_CLASSIC,
  GAME_ROUND_OVER,
  GAME_LIVES_SCREEN,
  GAME_OVER,
  GAME_ANIMATION_TEST,
} game_state_e;

game_controller game_controller__init_player_details(player_s *player1, player_s *player2);
void trigger_game_state_machine_and_get_ticks_to_wait(game_controller *game_contr);

/*
tick increase
tick players
check if player should move
check if crashed
    end if either crashed
        **Play explosion at new coordinate
move player
    delete where they were
    redraw at new location
    draw tail line
*/
