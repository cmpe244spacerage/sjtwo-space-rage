#pragma once

#include "game_board.h"

#include <stdbool.h>

typedef enum {
  ONE_PLAYER,
  TWO_PLAYER,
  //  OPTION_SELECT, /* TODO: ADD PLAYER COLOR OPTION */
} game_player_option;

typedef enum {
  CLASSIC_MODE,
  TURBO_MODE,
  //  OPTION_SELECT, /* TODO: ADD PLAYER COLOR OPTION */
} game_mode_option;

void game_display__draw_title_player_cursor(player_s *player, bool drawCmd);
bool game_display__move_title_player_cursor(player_s *player, bool move_cursor_down);
game_mode_option get_player_cursor_position(player_s *player);

void game_display__draw_player_cursor(location top_left_pixel_location, pixel_color_e color,
                                      current_direction_e facing_direction);