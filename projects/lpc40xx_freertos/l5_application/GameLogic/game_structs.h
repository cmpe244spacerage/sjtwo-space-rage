#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef enum {
  PLAYER1 = 1,
  PLAYER2 = 2,
} player_e;

typedef enum {
  NORTH = 0,
  EAST = 1,
  SOUTH = 2,
  WEST = 3,
} current_direction_e;

typedef enum {
  HUMAN = 1,
  AI = 2,
} player_type_e;

typedef enum {
  FAST = 2,
  SLOW = 4,
} player_speed_e;

typedef enum {
  EASY = 1,
  NORMAL = 2,
  HARD = 3,
} ai_difficulty_e;

typedef struct {
  uint8_t x_pos;
  uint8_t y_pos;
} location;

typedef enum {
  BLACK,
  BLUE,
  GREEN,
  CYAN,
  RED,
  PURPLE,
  YELLOW,
  WHITE,
  TRANSPARENT,
} pixel_color_e;

int8_t NORTH_DIRECTION;
int8_t EAST_DIRECTION;
int8_t SOUTH_DIRECTION;
int8_t WEST_DIRECTION;

location game_structs__add_location_offsets(location start_location, char *offsets, uint8_t size_of_offsets);
location game_structs__add_location_offsets_by_row_col(location start_location, int x, int y);