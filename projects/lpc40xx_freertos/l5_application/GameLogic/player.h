#pragma once
#include <gpio.h>
#include <stdint.h>

#include "game_structs.h"
#include "led_display.h"

typedef struct {
  gpio_s up;
  gpio_s down;
  gpio_s left;
  gpio_s right;
  gpio_s button;
} controller_pins;

typedef struct {
  location current_location;
  current_direction_e drawn_direction;
  current_direction_e new_direction;
  pixel_color_e player_color;
  player_speed_e current_speed;
  uint8_t ticks_till_move;
  uint8_t lives;
  uint8_t ai_logic_state;
} player_object;

typedef struct {
  player_type_e player_type;
  controller_pins *controller;
  ai_difficulty_e ai_difficulty;
  player_object player_details;
  bool has_crashed;
  uint8_t player_number;

} player_s;

player_s player__human_init(pixel_color_e player_color, controller_pins *controller, uint8_t player_number);
player_s player__ai_init(pixel_color_e player_color, ai_difficulty_e difficulty, uint8_t player_number);

void player__init_location(player_s *player_in, uint8_t player_number);
void player__controller_init(controller_pins *controller, gpio_s up, gpio_s right, gpio_s down, gpio_s left);

bool player__tick(player_s *player);
void player__update_location(player_s *player);
void player__check_for_crash(player_s *player);
