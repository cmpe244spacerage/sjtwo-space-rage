#include "FreeRTOS.h"
#include "task.h"
#include <stdlib.h>

#include "game_board.h"
#include "mp3.h"

void game_board__remove_ship(location current_location, current_direction_e drawn_direction,
                             current_direction_e new_direction, pixel_color_e color) {

  switch (drawn_direction) {
  case NORTH:

    game_board__clear_cell(game_structs__add_location_offsets(current_location, "N", 1));
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "W", 1));
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "E", 1));
    if (new_direction == EAST) {
      game_board__draw_cell(game_structs__add_location_offsets(current_location, "W", 1), color);
    }
    if (new_direction == WEST) {
      game_board__draw_cell(game_structs__add_location_offsets(current_location, "E", 1), color);
    }

    break;
  case EAST:
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "E", 1));
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "S", 1));
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "N", 1));
    if (new_direction == SOUTH) {
      game_board__draw_cell(game_structs__add_location_offsets(current_location, "N", 1), color);
    }
    if (new_direction == NORTH) {
      game_board__draw_cell(game_structs__add_location_offsets(current_location, "S", 1), color);
    }

    break;
  case SOUTH:
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "E", 1));
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "S", 1));
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "W", 1));
    if (new_direction == EAST) {
      game_board__draw_cell(game_structs__add_location_offsets(current_location, "W", 1), color);
    }
    if (new_direction == WEST) {
      game_board__draw_cell(game_structs__add_location_offsets(current_location, "E", 1), color);
    }

    break;
  case WEST:
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "W", 1));
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "S", 1));
    game_board__clear_cell(game_structs__add_location_offsets(current_location, "N", 1));
    if (new_direction == SOUTH) {
      game_board__draw_cell(game_structs__add_location_offsets(current_location, "N", 1), color);
    }
    if (new_direction == NORTH) {
      game_board__draw_cell(game_structs__add_location_offsets(current_location, "S", 1), color);
    }

    break;
  }
}
void game_board__draw_ship(location current_location, current_direction_e new_direction, pixel_color_e color) {

  switch (new_direction) {
  case NORTH:

    // head
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "N", 1), WHITE);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "E", 1), WHITE);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "W", 1), WHITE);
    // tail
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "S", 1), color);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "SE", 2), color);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "SW", 2), color);

    break;
  case EAST:

    // head
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "E", 1), WHITE);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "S", 1), WHITE);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "N", 1), WHITE);
    // tail
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "W", 1), color);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "WS", 2), color);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "WN", 2), color);

    break;
  case SOUTH:

    // head
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "S", 1), WHITE);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "E", 1), WHITE);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "W", 1), WHITE);
    // tail
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "N", 1), color);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "NE", 2), color);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "NW", 2), color);

    break;
  case WEST:

    // head
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "W", 1), WHITE);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "S", 1), WHITE);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "N", 1), WHITE);
    // tail
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "E", 1), color);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "ES", 2), color);
    game_board__draw_cell(game_structs__add_location_offsets(current_location, "EN", 2), color);
    break;
  }
}

void game_board__draw_cell(location location_to_draw, pixel_color_e color) {
  led_display__draw_pixel(location_to_draw.y_pos, location_to_draw.x_pos, color);
}
void game_board__clear_cell(location location_to_clear) {
  pixel_color_e CLEAR_CELL_BLACK = BLACK;
  game_board__draw_cell(location_to_clear, CLEAR_CELL_BLACK);
} // Possibly change to a location type input

void game_board__clear_cell_block_top_left_corner_target(location current_location, uint8_t cols, uint8_t rows) {
  location current_draw_location;

  for (int x = 0; x < cols; x++) {
    for (int y = 0; y < rows; y++) {
      current_draw_location.x_pos = current_location.x_pos + x;
      current_draw_location.y_pos = current_location.y_pos + y;
      game_board__clear_cell(current_draw_location);
    }
  }
}

void game_board__clear_cell_block_center_target(location current_location, uint8_t cols, uint8_t rows) {
  location top_left_corner_location;
  top_left_corner_location.x_pos = current_location.x_pos - cols / 2;
  top_left_corner_location.y_pos = current_location.y_pos - rows / 2;
  location current_draw_location;

  for (int x = 0; x < cols; x++) {
    for (int y = 0; y < rows; y++) {
      current_draw_location.x_pos = top_left_corner_location.x_pos + x;
      current_draw_location.y_pos = top_left_corner_location.y_pos + y;
      game_board__clear_cell(current_draw_location);
    }
  }
}

bool game_board__check_cell(location current_location) {
  // true if there is something there
  pixel_color_e empty_cell = BLACK;

  if (current_location.y_pos < 0 || current_location.y_pos > 63 || current_location.x_pos < 0 ||
      current_location.x_pos > 63) {
    return true;
  }

  if (led_display__get_pixel(current_location.y_pos, current_location.x_pos) != empty_cell)
    return true;
  return false;
};
bool game_board__check_forward_for_player(location current_location, current_direction_e new_direction) {
  bool if_any_pixel_not_open = false;
  switch (new_direction) {
  case NORTH:
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "NN", 2));
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "NNE", 3));
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "NNW", 3));
    break;
  case EAST:
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "EE", 2));
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "EEN", 3));
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "EES", 3));

    break;
  case SOUTH:
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "SS", 2));
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "SSE", 3));
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "SSW", 3));
    break;
  case WEST:
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "WW", 2));
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "WWN", 3));
    if_any_pixel_not_open |= game_board__check_cell(game_structs__add_location_offsets(current_location, "WWS", 3));
    break;
  }
  return if_any_pixel_not_open;
};

void game_board__draw_border(pixel_color_e border_color) {
  for (uint8_t y = 0; y < 64; y++) {
    for (uint8_t x = 0; x < 64; x++) {
      if (0 == x || 63 == x) {
        led_display__draw_pixel(y, x, border_color);
      } else if (0 == y || 63 == y) {
        led_display__draw_pixel(y, x, border_color);
      }
    }
  }
}

void game_board__draw_screen(uint8_t screen_image[64][64]) {
  location top_left_pixel = {0, 0};
  location moving_cursor_main_screen = top_left_pixel;
  for (uint8_t y = 0; y < 64; y++) {
    moving_cursor_main_screen.y_pos = top_left_pixel.y_pos + y;
    for (uint8_t x = 0; x < 64; x++) {
      moving_cursor_main_screen.x_pos = top_left_pixel.x_pos + x;
      pixel_color_e pixel_to_draw = game_board__translate_abriviation(screen_image[y][x]);
      if (pixel_to_draw != TRANSPARENT && moving_cursor_main_screen.x_pos <= 63 &&
          moving_cursor_main_screen.y_pos <= 63 && moving_cursor_main_screen.y_pos >= 0 &&
          moving_cursor_main_screen.x_pos >= 0) {
        game_board__draw_cell(moving_cursor_main_screen, pixel_to_draw);
      }
    }
  }
}

pixel_color_e game_board__translate_abriviation(uint8_t char_in) {
  pixel_color_e color_out = BLACK;
  switch (char_in) {
  case 'r':
    color_out = RED;
    break;
  case 'g':
    color_out = GREEN;
    break;
  case 'b':
    color_out = BLUE;
    break;
  case 'y':
    color_out = YELLOW;
    break;
  case 'p':
    color_out = PURPLE;
    break;
  case 'c':
    color_out = CYAN;
    break;
  case 'w':
    color_out = WHITE;
    break;
  case 't':
    color_out = TRANSPARENT;
    break;

  default:
    break;
  }
  return color_out;
}

void game_board__draw_explosion(location target_location) {

  game_board__draw_image_center_target(1, 1, explosion__frame_1, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(3, 3, explosion__frame_2, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(5, 5, explosion__frame_3, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(7, 7, explosion__frame_4, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(7, 7, explosion__frame_5, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(7, 7, explosion__frame_6, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(7, 7, explosion__frame_7, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(7, 7, explosion__frame_8, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(7, 7, explosion__frame_9, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(7, 7, explosion__frame_10, target_location);
  vTaskDelay(100);
  game_board__draw_image_center_target(7, 7, explosion__frame_11, target_location);
  vTaskDelay(100);
}
void game_board__draw_lives_screen(uint8_t player1_lives, uint8_t player2_lives, bool player1_has_crashed,
                                   bool player2_has_crashed) {
  game_display__empty_screen();
  game_board__draw_border(CYAN);
  location player_1_location = {3, 16};
  location player_2_location = {31, 16};
  game_board__draw_image_top_left_target(10, 28, lives_screen__player_1, player_1_location);
  game_board__draw_image_top_left_target(10, 28, lives_screen__player_2, player_2_location);

  location player1_heart_center_location = {8, 29};
  location player2_heart_center_location = {36, 29};
  if (player1_has_crashed) {
    player1_lives++;
  }
  if (player2_has_crashed) {
    player2_lives++;
  }
  for (int player1_hearts = 0; player1_hearts < player1_lives; player1_hearts++) {
    location player1_heart_to_draw;
    player1_heart_to_draw.x_pos = player1_heart_center_location.x_pos + (player1_hearts * 7);
    player1_heart_to_draw.y_pos = player1_heart_center_location.y_pos;
    game_board__draw_image_center_target(5, 5, lives_screen__heart_blue, player1_heart_to_draw);
  }
  for (int player2_hearts = 0; player2_hearts < player2_lives; player2_hearts++) {
    location player2_heart_to_draw;
    player2_heart_to_draw.x_pos = player2_heart_center_location.x_pos + (player2_hearts * 7);
    player2_heart_to_draw.y_pos = player2_heart_center_location.y_pos;
    game_board__draw_image_center_target(5, 5, lives_screen__heart_red, player2_heart_to_draw);
  }
  if (player1_has_crashed) { // animation
    vTaskDelay(1000);
    location player1_last_heart;
    player1_last_heart.x_pos = player1_heart_center_location.x_pos + ((player1_lives - 1) * 7);
    player1_last_heart.y_pos = player1_heart_center_location.y_pos;

    /*
    uint8_t fadeTimerOn = 50;
    uint8_t fadeTimerOff = 0;
        while (fadeTimerOn > 0) {
          game_board__draw_image_center_target(5, 5, lives_screen__heart_blue, player1_last_heart);
          vTaskDelay(fadeTimerOn);
          game_board__clear_cell_block_center_target(player1_last_heart, 5, 5);
          vTaskDelay(fadeTimerOff);

          fadeTimerOn = fadeTimerOn - 1;
          fadeTimerOff = fadeTimerOff + 1;
        }
        */

    // THIS LOOKS PRETTY COOL
    uint8_t fadeTimerOn = 50;
    uint8_t fadeTimerOff = 0;
    while (fadeTimerOn > 0) {
      game_board__draw_image_center_target(5, 5, lives_screen__heart_blue, player1_last_heart);
      vTaskDelay(fadeTimerOn);
      game_board__clear_cell_block_center_target(player1_last_heart, 5, 5);
      vTaskDelay(fadeTimerOff);

      fadeTimerOn = fadeTimerOn - 5;
      fadeTimerOff = fadeTimerOff + 5;
    }

    game_board__draw_explosion(player1_last_heart);
  }

  if (player2_has_crashed) { // animation
    vTaskDelay(1000);
    location player2_last_heart;
    player2_last_heart.x_pos = player2_heart_center_location.x_pos + ((player2_lives - 1) * 7);
    player2_last_heart.y_pos = player2_heart_center_location.y_pos;
    uint8_t fadeTimerOn = 50;
    uint8_t fadeTimerOff = 10;

    while (fadeTimerOn > 0) {
      game_board__draw_image_center_target(5, 5, lives_screen__heart_red, player2_last_heart);
      vTaskDelay(fadeTimerOn);
      game_board__clear_cell_block_center_target(player2_last_heart, 5, 5);
      vTaskDelay(fadeTimerOff);

      fadeTimerOn = fadeTimerOn - 1;
      // fadeTimerOff = fadeTimerOff + 1;
    }
  }
  vTaskDelay(2000);
  // == 1 due to prior offset
  if (player1_has_crashed && player1_lives == 1 && player2_has_crashed && player2_lives == 1) {
    game_display__empty_screen();
    game_board__draw_border(CYAN);
    location draw_location = {17, 23};
    mp3__uart3_send_cmd(draw);
    game_board__draw_image_top_left_target(10, 28, lives_screen__draw, draw_location);
    vTaskDelay(3000);
  } else if (player1_has_crashed && player1_lives == 1) {
    game_display__empty_screen();
    game_board__draw_border(CYAN);
    player_2_location.x_pos = 3;
    player_2_location.y_pos = 29;
    location wins_location = {32, 29};
    mp3__uart3_send_cmd(p2_wins);
    game_board__draw_image_top_left_target(10, 28, lives_screen__player_2, player_2_location);
    game_board__draw_image_top_left_target(10, 28, lives_screen__wins, wins_location);
    vTaskDelay(3000);
  } else if (player2_has_crashed && player2_lives == 1) {
    game_display__empty_screen();
    game_board__draw_border(CYAN);
    player_1_location.x_pos = 3;
    player_1_location.y_pos = 29;
    location wins_location = {32, 29};
    mp3__uart3_send_cmd(p1_wins);
    game_board__draw_image_top_left_target(10, 28, lives_screen__player_1, player_1_location);
    game_board__draw_image_top_left_target(10, 28, lives_screen__wins, wins_location);
    vTaskDelay(3000);
  }
}

void game_board__draw_title(void) {
  game_display__empty_screen();
  location draw_location = {0, 0};
  for (int x = 0; x < 2; x++) {
    draw_location.x_pos = x;
    for (int y = 0; y < 64; y++) {
      draw_location.y_pos = y;
      game_board__draw_cell(draw_location, BLUE);
    }
  }
  for (int x = 62; x < 64; x++) {
    draw_location.x_pos = x;
    for (int y = 0; y < 64; y++) {
      draw_location.y_pos = y;
      game_board__draw_cell(draw_location, RED);
    }
  }
  draw_location.x_pos = 19;
  draw_location.y_pos = 4;
  game_board__draw_image_top_left_target(10, 28, game_menu__space, draw_location);
  draw_location.x_pos = 22;
  draw_location.y_pos = 12;
  game_board__draw_image_top_left_target(7, 21, game_menu__rage, draw_location);
}

void game_board__draw_image_top_left_target(uint8_t row_count, uint8_t col_count,
                                            uint8_t image_name[row_count][col_count], location top_left_pixel) {

  location moving_cursor_main_screen = top_left_pixel;

  for (uint8_t x = 0; x < col_count; x++) {
    moving_cursor_main_screen.x_pos = top_left_pixel.x_pos + x;
    for (uint8_t y = 0; y < row_count; y++) {
      moving_cursor_main_screen.y_pos = top_left_pixel.y_pos + y;
      pixel_color_e pixel_to_draw = game_board__translate_abriviation(image_name[y][x]);
      pixel_color_e target_pixel =
          led_display__get_pixel(moving_cursor_main_screen.y_pos, moving_cursor_main_screen.x_pos);
      if (pixel_to_draw != TRANSPARENT && target_pixel != PURPLE) {
        game_board__draw_cell(moving_cursor_main_screen, pixel_to_draw);
      }
    }
  }
}

void game_board__draw_image_center_target(uint8_t row_count, uint8_t col_count,
                                          uint8_t image_name[row_count][col_count], location center_pixel) {
  location top_left_pixel = {0, 0};
  top_left_pixel.x_pos = center_pixel.x_pos - (col_count / 2); // Move up
  top_left_pixel.y_pos = center_pixel.y_pos - (row_count / 2); // Move left

  location moving_cursor_main_screen = top_left_pixel;

  for (uint8_t x = 0; x < col_count; x++) {
    moving_cursor_main_screen.x_pos = top_left_pixel.x_pos + x;
    for (uint8_t y = 0; y < row_count; y++) {
      moving_cursor_main_screen.y_pos = top_left_pixel.y_pos + y;
      pixel_color_e pixel_to_draw = game_board__translate_abriviation(image_name[y][x]);
      pixel_color_e target_pixel =
          led_display__get_pixel(moving_cursor_main_screen.y_pos, moving_cursor_main_screen.x_pos);
      if (pixel_to_draw != TRANSPARENT && target_pixel != PURPLE && moving_cursor_main_screen.x_pos > 0 &&
          moving_cursor_main_screen.x_pos < 64 && moving_cursor_main_screen.y_pos > 0 &&
          moving_cursor_main_screen.y_pos < 64) {
        game_board__draw_cell(moving_cursor_main_screen, pixel_to_draw);
      }
    }
  }
}

void game_display__empty_screen(void) {

  for (uint8_t row = 0; row < 64; row++) {
    for (uint8_t col = 0; col < 64; col++) {
      led_display__draw_pixel(row, col, BLACK);
    }
  }
}

void game_board__draw_countdown(void) {
  int delay = 40;
  int delay2 = 350;
  int delay3 = 20;
  location center = {31, 31};
  game_board__draw_image_center_target(8, 5, countdown__3_block_size_1, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(16, 10, countdown__3_block_size_2, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(24, 15, countdown__3_block_size_3, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(32, 20, countdown__3_block_size_4, center);
  vTaskDelay(delay2);
  game_board__draw_image_center_target(32, 32, countdown__clear_number, center);
  vTaskDelay(delay3);

  game_board__draw_image_center_target(8, 5, countdown__2_block_size_1, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(16, 10, countdown__2_block_size_2, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(24, 15, countdown__2_block_size_3, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(32, 20, countdown__2_block_size_4, center);
  vTaskDelay(delay2);
  game_board__draw_image_center_target(32, 32, countdown__clear_number, center);
  vTaskDelay(delay3);

  game_board__draw_image_center_target(8, 3, countdown__1_block_size_1, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(16, 6, countdown__1_block_size_2, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(24, 9, countdown__1_block_size_3, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(32, 12, countdown__1_block_size_4, center);
  vTaskDelay(delay2);
  game_board__draw_image_center_target(32, 32, countdown__clear_number, center);
  vTaskDelay(delay3);

  game_board__draw_image_center_target(8, 13, countdown__go_block_size_1, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(16, 26, countdown__go_block_size_2, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(24, 39, countdown__go_block_size_3, center);
  vTaskDelay(delay);
  game_board__draw_image_center_target(32, 52, countdown__go_block_size_4, center);
  vTaskDelay(delay2);
  game_board__draw_image_center_target(32, 52, countdown__go_clear_for_game, center);
  vTaskDelay(delay3);
}
void game_board__draw_volume(void);