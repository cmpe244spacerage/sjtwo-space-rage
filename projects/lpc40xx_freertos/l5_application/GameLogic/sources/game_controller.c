
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "board_io.h"
#include "delay.h"

#include "game_controller.h"
#include "mp3.h"
#include "player_joysticks.h"

static void game_controller__reset_rounds(game_controller *game_contr);
static bool game_controller__game_over(game_controller *game_contr);
static bool game_controller__start_game(game_controller *game_contr);
static void game_controller__tick(game_controller *game_contr);
static void game_controller__move_player(player_s *player_to_move);

static void game_controller__process_user_game_movement(player_s *player_num, current_direction_e player_input);
static void game_controller__process_user_cursor_movement(player_s *player_num, current_direction_e player_input);
static void game_controller__init_round(game_controller *game_contr);

/*******************************************************************************
 *
 *                      P U B L I C    F U N C T I O N S
 *
 ******************************************************************************/
game_controller game_controller__init_player_details(player_s *player1, player_s *player2) {
  game_controller game_out;
  game_out.player1 = player1;
  game_out.player2 = player2;
  game_out.player1->player_type = HUMAN;
  game_out.player2->player_type = HUMAN;
  game_out.currentTick = 0;
  game_out.isPaused = true;
  game_out.isFinished = false;
  game_out.player1->has_crashed = false;
  game_out.player2->has_crashed = false;
  game_out.player1->player_details.ai_logic_state = 0;
  game_out.player2->player_details.ai_logic_state = 0;
  srand(13521);
  return game_out;
}

void trigger_game_state_machine_and_get_ticks_to_wait(game_controller *game_contr) {
  static game_state_e current_game_state = GAME_TITLE;
  uint8_t player_one_input, player_two_input;
  gpio_s player_one_button = {.port_number = 0, .pin_number = 22};
  gpio_s player_two_button = {.port_number = 0, .pin_number = 1};
  static game_mode_option game_mode = GAME_CLASSIC;
  uint32_t after_explosion_delay_timer = 1000;

  static uint32_t time_for_volume_to_show = 0;
  const uint32_t max_count_for_volume_to_show = 50;

  static uint32_t current_volume_check_count = 0;
  const uint32_t count_to_check_for_volume = 5;

  switch (current_game_state) {

  case GAME_TITLE:
    game_board__draw_title();
    mp3__uart3_send_cmd(space_rage);
    vTaskDelay(1000);
    mp3__uart3_send_cmd(title_screen);
    current_game_state = GAME_PLAYER_SELECTION_DRAW;
    break;
  case GAME_PLAYER_SELECTION_DRAW:
    game_board__draw_title();
    game_contr->player1->player_type = HUMAN;
    game_contr->player2->player_type = HUMAN;

    // Need player count select draw
    location one_player_location = {18, 27};
    game_board__draw_image_top_left_target(10, 28, game_menu__one_player, one_player_location);

    location two_player_location = {18, 41};
    game_board__draw_image_top_left_target(10, 28, game_menu__two_player, two_player_location);
    game_display__draw_title_player_cursor(game_contr->player1, true);
    game_display__draw_title_player_cursor(game_contr->player2, true);
    current_game_state = GAME_PLAYER_SELECTION;
    player_buttons__empty_semaphores();
    player_joysticks__empty_queue();
    break;

  case GAME_PLAYER_SELECTION:
    if (current_volume_check_count == count_to_check_for_volume) {
      current_volume_check_count = 0;
      if (!mp3__check_volume_buttons()) {
        time_for_volume_to_show++;
      }
    } else {
      current_volume_check_count++;
      time_for_volume_to_show++;
      if (time_for_volume_to_show >= max_count_for_volume_to_show) {
        time_for_volume_to_show = 0;
        mp3__clear_volume_indicator();
      }
    }

    if (player_joysticks__check_and_process_players_inputs_if_any(PLAYER1, &player_one_input)) {
      game_controller__process_user_cursor_movement(game_contr->player1, player_one_input);
    }
    if (player_joysticks__check_and_process_players_inputs_if_any(PLAYER2, &player_two_input)) {
      game_controller__process_user_cursor_movement(game_contr->player2, player_two_input);
    }
    if (player_joysticks__check_and_process_player_button(PLAYER1)) {
      if (get_player_cursor_position(game_contr->player1) == 0) {
        game_contr->player2->ai_difficulty = NORMAL;
        game_contr->player2->player_type = AI;

        game_contr->player1->player_type = HUMAN;

        mp3__uart3_send_cmd(single_player);
      } else {
        mp3__uart3_send_cmd(versus_mode);
      }

      current_game_state = GAME_MODE_SELECTION_DRAW;
    }
    if (player_joysticks__check_and_process_player_button(PLAYER2)) {
      if (get_player_cursor_position(game_contr->player2) == 0) {
        game_contr->player1->ai_difficulty = HARD;
        game_contr->player1->player_type = AI;

        game_contr->player2->player_type = HUMAN;
        mp3__uart3_send_cmd(single_player);
      } else {
        mp3__uart3_send_cmd(versus_mode);
      }

      current_game_state = GAME_MODE_SELECTION_DRAW;
    }
    vTaskDelay(100);
    break;
  case GAME_MODE_SELECTION_DRAW:
    game_board__draw_title();
    location classic_location = {18, 27};
    game_board__draw_image_top_left_target(10, 28, game_menu__classic, classic_location);

    location turbo_locaiton = {18, 41};
    game_board__draw_image_top_left_target(10, 28, game_menu__turbo, turbo_locaiton);
    game_display__draw_title_player_cursor(game_contr->player1, true);
    game_display__draw_title_player_cursor(game_contr->player2, true);
    current_game_state = GAME_MODE_SELECTION;
    player_buttons__empty_semaphores();
    player_joysticks__empty_queue();
    break;
  case GAME_MODE_SELECTION:
    if (current_volume_check_count == count_to_check_for_volume) {
      current_volume_check_count = 0;
      if (!mp3__check_volume_buttons()) {
        time_for_volume_to_show++;
      }
    } else {
      current_volume_check_count++;
      time_for_volume_to_show++;
      if (time_for_volume_to_show >= max_count_for_volume_to_show) {
        time_for_volume_to_show = 0;
        mp3__clear_volume_indicator();
      }
    }

    if (player_joysticks__check_and_process_players_inputs_if_any(PLAYER1, &player_one_input)) {
      game_controller__process_user_cursor_movement(game_contr->player1, player_one_input);
    }
    if (player_joysticks__check_and_process_players_inputs_if_any(PLAYER2, &player_two_input)) {
      game_controller__process_user_cursor_movement(game_contr->player2, player_two_input);
    }
    /* Else if here, because we only want one player to input game mode */
    if (player_joysticks__check_and_process_player_button(PLAYER1)) {
      game_mode = get_player_cursor_position(game_contr->player1);
      game_mode == CLASSIC_MODE ? mp3__uart3_send_cmd(classic_mode) : mp3__uart3_send_cmd(turbo_mode);
      current_game_state = GAME_RESET_LIVES;
      vTaskDelay(3000);
    } else if (player_joysticks__check_and_process_player_button(PLAYER2)) {
      game_mode = get_player_cursor_position(game_contr->player2);
      game_mode == CLASSIC_MODE ? mp3__uart3_send_cmd(classic_mode) : mp3__uart3_send_cmd(turbo_mode);
      current_game_state = GAME_RESET_LIVES;
      vTaskDelay(3000);
    }
    vTaskDelay(100);

    break;
  case GAME_RESET_LIVES:
    game_controller__reset_rounds(game_contr);
    current_game_state = GAME_ROUND_INIT;
    break;
  case GAME_ROUND_INIT:
    game_contr->player1->player_details.ai_logic_state = 0;
    game_contr->player2->player_details.ai_logic_state = 0;
    game_display__empty_screen();
    game_board__draw_border(PURPLE);
    game_controller__init_round(game_contr);
    mp3__uart3_send_cmd(stop_music);
    if (game_controller__start_game(game_contr)) {
      mp3__uart3_send_cmd(game_music);
      vTaskDelay(2050);
      mp3__uart3_send_cmd(count_sequence);
      game_board__draw_countdown();
      current_game_state = game_mode == CLASSIC_MODE ? GAME_CLASSIC : GAME_TURBO;
    } else {
      current_game_state = GAME_TITLE;
    }
    player_joysticks__empty_queue();
    break;
  case GAME_TURBO:
    if (game_contr->player1->player_type == HUMAN) {
      if (gpio__get(player_one_button)) {
        game_contr->player1->player_details.current_speed = FAST;
      } else {
        game_contr->player1->player_details.current_speed = SLOW;
      }
    } else if (game_contr->player1->player_type == AI) {
      if (game_contr->player1->ai_difficulty == HARD && game_contr->player1->player_details.ai_logic_state == 0) {
        game_contr->player1->player_details.current_speed = FAST;
      } else {
        game_contr->player1->player_details.current_speed = SLOW;
      }
    }

    if (game_contr->player2->player_type == HUMAN) {
      if (gpio__get(player_two_button)) {
        game_contr->player2->player_details.current_speed = FAST;
      } else {
        game_contr->player2->player_details.current_speed = SLOW;
      }
    }

  case GAME_CLASSIC:
    if (game_contr->player1->player_type == HUMAN) {
      if (player_joysticks__check_and_process_players_inputs_if_any(PLAYER1, &player_one_input)) {
        game_controller__process_user_game_movement(game_contr->player1, player_one_input);
      }
    } else if (game_contr->player1->player_type == AI) {
      ai_base__ai_decide(game_contr->player1, game_contr->player2->player_details.current_location);
    }
    if (game_contr->player2->player_type == HUMAN) {
      if (player_joysticks__check_and_process_players_inputs_if_any(PLAYER2, &player_two_input)) {
        game_controller__process_user_game_movement(game_contr->player2, player_two_input);
      }
    } else if (game_contr->player2->player_type == AI) {
      ai_base__ai_decide(game_contr->player2, game_contr->player1->player_details.current_location);
    }

    game_controller__tick(game_contr);
    if (game_controller__game_over(game_contr)) {
      current_game_state = GAME_ROUND_OVER;
    }
    vTaskDelay(20);
    break;
  case GAME_ROUND_OVER:
    if (game_contr->player1->has_crashed) {
      game_contr->player1->player_details.lives--;
      mp3__uart3_send_cmd(p1_explosion);
      game_board__draw_explosion(game_contr->player1->player_details.current_location);
      vTaskDelay(after_explosion_delay_timer);
    }
    if (game_contr->player2->has_crashed) {
      game_contr->player2->player_details.lives--;
      mp3__uart3_send_cmd(p2_explosion);
      game_board__draw_explosion(game_contr->player2->player_details.current_location);
      vTaskDelay(after_explosion_delay_timer);
    }
    current_game_state = GAME_LIVES_SCREEN;
    mp3__uart3_send_cmd(stop_music);

    vTaskDelay(100);
    break;
  case GAME_LIVES_SCREEN:
    mp3__uart3_send_cmd(lives_music);
    game_board__draw_lives_screen(game_contr->player1->player_details.lives, game_contr->player2->player_details.lives,
                                  game_contr->player1->has_crashed, game_contr->player2->has_crashed);
    if (game_contr->player1->player_details.lives == 0 || game_contr->player2->player_details.lives == 0) {
      current_game_state = GAME_OVER;
    } else {
      current_game_state = GAME_ROUND_INIT;
    }
    break;
  case GAME_OVER:
    current_game_state = GAME_TITLE;

    break;
  case GAME_ANIMATION_TEST:
    game_board__draw_countdown();
  }
}
/*******************************************************************************
 *
 *                      E N D  O F  P U B L I C    F U N C T I O N S
 *
 ******************************************************************************/
/*******************************************************************************
 *
 *                      P R I V A T E    F U N C T I O N S
 *
 ******************************************************************************/
static void game_controller__reset_rounds(game_controller *game_contr) {
  const uint8_t max_player_lives = 3;

  game_contr->isGameOver = false;
  game_contr->player1->player_details.lives = max_player_lives;
  game_contr->player2->player_details.lives = max_player_lives;
}

static bool game_controller__game_over(game_controller *game_contr) {
  if (game_contr->player1->has_crashed == true || game_contr->player2->has_crashed == true) {
    game_contr->isFinished = true;
  }
  return game_contr->isFinished;
}

static bool game_controller__start_game(game_controller *game_out) {
  bool game_ready_to_start = false;
  game_out->isPaused = false;
  if (game_out->player1 != NULL && game_out->player2 != NULL) {
    game_ready_to_start = true;
  }
  return game_ready_to_start;
}

static void game_controller__tick(game_controller *game_contr) {
  if (game_contr->isPaused != true && game_contr->isFinished != true) {
    if (player__tick(game_contr->player1)) {
      if (game_board__check_forward_for_player(game_contr->player1->player_details.current_location,
                                               game_contr->player1->player_details.new_direction)) {
        game_contr->player1->has_crashed = true;
      } else {

        game_controller__move_player(game_contr->player1);
      }
    }
    if (player__tick(game_contr->player2)) {
      if (game_board__check_forward_for_player(game_contr->player2->player_details.current_location,
                                               game_contr->player2->player_details.new_direction)) {
        game_contr->player2->has_crashed = true;
      } else {

        game_controller__move_player(game_contr->player2);
      }
    }
  }
}

static void game_controller__move_player(player_s *player_to_move) {
  // Update central location in player

  game_board__remove_ship(player_to_move->player_details.current_location,
                          player_to_move->player_details.drawn_direction, player_to_move->player_details.new_direction,
                          player_to_move->player_details.player_color);

  player__update_location(player_to_move);

  game_board__draw_ship(player_to_move->player_details.current_location, player_to_move->player_details.new_direction,
                        player_to_move->player_details.player_color);

  player_to_move->player_details.drawn_direction = player_to_move->player_details.new_direction;
}

static void game_controller__process_user_game_movement(player_s *player_num, current_direction_e player_input) {

  switch (player_input) {
  case NORTH:
    if (player_num->player_details.drawn_direction != SOUTH && player_num->player_details.drawn_direction != NORTH) {
      // player_num->player_details.drawn_direction = player_input;
      player_num->player_details.new_direction = player_input;
      player_num->player_number == PLAYER1 ? mp3__uart3_send_cmd(p1_game_movement)
                                           : mp3__uart3_send_cmd(p2_game_movement);
    }
    break;
  case SOUTH:
    if (player_num->player_details.drawn_direction != NORTH && player_num->player_details.drawn_direction != SOUTH) {
      // player_num->player_details.drawn_direction = player_input;
      player_num->player_details.new_direction = player_input;
      player_num->player_number == PLAYER1 ? mp3__uart3_send_cmd(p1_game_movement)
                                           : mp3__uart3_send_cmd(p2_game_movement);
    }
    break;
  case WEST:
    if (player_num->player_details.drawn_direction != EAST && player_num->player_details.drawn_direction != WEST) {
      // player_num->player_details.drawn_direction = player_input;
      player_num->player_details.new_direction = player_input;
      player_num->player_number == PLAYER1 ? mp3__uart3_send_cmd(p1_game_movement)
                                           : mp3__uart3_send_cmd(p2_game_movement);
    }
    break;
  case EAST:
    if (player_num->player_details.drawn_direction != WEST && player_num->player_details.drawn_direction != EAST) {
      // player_num->player_details.drawn_direction = player_input;
      player_num->player_details.new_direction = player_input;
      player_num->player_number == PLAYER1 ? mp3__uart3_send_cmd(p1_game_movement)
                                           : mp3__uart3_send_cmd(p2_game_movement);
    }
    break;
  }
}

static void game_controller__process_user_cursor_movement(player_s *player_num, current_direction_e player_input) {
  switch (player_input) {
  case NORTH:
    if (game_display__move_title_player_cursor(player_num, false)) {
      player_num->player_number == PLAYER1 ? mp3__uart3_send_cmd(p1_menu_cursor) : mp3__uart3_send_cmd(p2_menu_cursor);
    }
    break;
  case SOUTH:
    if (game_display__move_title_player_cursor(player_num, true)) {
      player_num->player_number == PLAYER1 ? mp3__uart3_send_cmd(p1_menu_cursor) : mp3__uart3_send_cmd(p2_menu_cursor);
    }
    break;
  case EAST:
  case WEST:
    break;
  }
}

static void game_controller__init_round(game_controller *game_contr) {
  game_contr->currentTick = 0;
  game_contr->isPaused = true;
  game_contr->isFinished = false;
  game_contr->player1->has_crashed = false;
  game_contr->player2->has_crashed = false;
  game_contr->player1->player_details.current_speed = SLOW;
  game_contr->player2->player_details.current_speed = SLOW;
  game_contr->player1->player_details.ticks_till_move = SLOW;
  game_contr->player2->player_details.ticks_till_move = SLOW;

  location player1_start_location = {3, 3};
  location player2_start_location = {60, 60};

  game_contr->player1->player_details.current_location = player1_start_location;
  game_contr->player2->player_details.current_location = player2_start_location;

  game_contr->player1->player_details.drawn_direction = SOUTH;
  game_contr->player2->player_details.drawn_direction = NORTH;
  game_contr->player1->player_details.new_direction = SOUTH;
  game_contr->player2->player_details.new_direction = NORTH;

  game_board__draw_ship(game_contr->player1->player_details.current_location,
                        game_contr->player1->player_details.drawn_direction,
                        game_contr->player1->player_details.player_color);

  game_board__draw_ship(game_contr->player2->player_details.current_location,
                        game_contr->player2->player_details.drawn_direction,
                        game_contr->player2->player_details.player_color);
}
/*******************************************************************************
 *
 *               E N D    O F    P R I V A T E    F U N C T I O N S
 *
 ******************************************************************************/