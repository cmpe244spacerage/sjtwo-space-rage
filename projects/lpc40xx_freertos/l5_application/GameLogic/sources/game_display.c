#include "game_display.h"
#include "game_structs.h"

const pixel_color_e x = PURPLE;
const pixel_color_e v = CYAN;
const pixel_color_e e = RED;
const pixel_color_e p = BLUE;

const static uint8_t MAX_CURSOR_OPTIONS = 2;
static uint8_t player_one_cursor_index = 0;
static uint8_t player_two_cursor_index = 0;
static game_mode_option player_one_position[] = {CLASSIC_MODE, TURBO_MODE};
static game_mode_option player_two_position[] = {CLASSIC_MODE, TURBO_MODE};

/*
  c is the tip of the cursor and is used as a reference point to draw the cursor

  x,                              ,x
  x,x,                           x,x
  x,x,c                        c,x,x
  x,x,                           x,x
  x,                              ,x

*/

void game_display__draw_title_player_cursor(player_s *player, bool drawCmd) {
  pixel_color_e color_to_draw = drawCmd ? player->player_details.player_color : BLACK;
  game_mode_option position = (player->player_number == PLAYER1) ? player_one_position[player_one_cursor_index]
                                                                 : player_two_position[player_two_cursor_index];
  location top_left_draw_pixel = {0};
  current_direction_e facing_direction = EAST;
  if (PLAYER1 == player->player_number) {
    facing_direction = EAST;
    switch (position) {
    case CLASSIC_MODE:
      top_left_draw_pixel.x_pos = 9;
      top_left_draw_pixel.y_pos = 28;
      break;

    case TURBO_MODE:
      top_left_draw_pixel.x_pos = 9;
      top_left_draw_pixel.y_pos = 43;
      break;
    }
  }
  if (PLAYER2 == player->player_number) {
    facing_direction = WEST;
    switch (position) {
    case CLASSIC_MODE:
      top_left_draw_pixel.x_pos = 51;
      top_left_draw_pixel.y_pos = 28;
      break;

    case TURBO_MODE:
      top_left_draw_pixel.x_pos = 51;
      top_left_draw_pixel.y_pos = 43;
      break;
    }
  }
  game_display__draw_player_cursor(top_left_draw_pixel, color_to_draw, facing_direction);
}

bool game_display__move_title_player_cursor(player_s *player, bool move_cursor_down) {
  bool draw_color = true;
  bool cursor_moved = false;
  uint8_t *cursor_index_ptr = player->player_number == PLAYER1 ? &player_one_cursor_index : &player_two_cursor_index;

  if (move_cursor_down) {
    /* Don't move the cursor down anymore if at the last option, in this case, it does not roll over */
    if (*cursor_index_ptr < (MAX_CURSOR_OPTIONS - 1)) {
      /* Erase Cursor */
      game_display__draw_title_player_cursor(player, !draw_color);
      (*cursor_index_ptr)++;
      game_display__draw_title_player_cursor(player, draw_color);
      cursor_moved = true;
    }
  } else {
    if (*cursor_index_ptr != 0) {
      /* Erase Cursor */
      game_display__draw_title_player_cursor(player, !draw_color);
      (*cursor_index_ptr)--;
      game_display__draw_title_player_cursor(player, draw_color);
      cursor_moved = true;
    }
  }

  return cursor_moved;
}

game_mode_option get_player_cursor_position(player_s *player) {
  game_mode_option position = 0;
  switch (player->player_number) {
  case PLAYER1:
    position = player_one_position[player_one_cursor_index];
    break;
  case PLAYER2:
    position = player_two_position[player_two_cursor_index];
    break;
  }
  return position;
}

void game_display__draw_player_cursor(location top_left_pixel_location, pixel_color_e color,
                                      current_direction_e facing_direction) {

  if (facing_direction == EAST) {
    led_display__draw_pixel(top_left_pixel_location.y_pos, top_left_pixel_location.x_pos, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 1, top_left_pixel_location.x_pos, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 2, top_left_pixel_location.x_pos, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 3, top_left_pixel_location.x_pos, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 4, top_left_pixel_location.x_pos, color);

    led_display__draw_pixel(top_left_pixel_location.y_pos + 1, top_left_pixel_location.x_pos + 1, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 2, top_left_pixel_location.x_pos + 1, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 3, top_left_pixel_location.x_pos + 1, color);

    led_display__draw_pixel(top_left_pixel_location.y_pos + 2, top_left_pixel_location.x_pos + 2, color);

  } else {
    led_display__draw_pixel(top_left_pixel_location.y_pos, top_left_pixel_location.x_pos + 2, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 1, top_left_pixel_location.x_pos + 2, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 2, top_left_pixel_location.x_pos + 2, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 3, top_left_pixel_location.x_pos + 2, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 4, top_left_pixel_location.x_pos + 2, color);

    led_display__draw_pixel(top_left_pixel_location.y_pos + 1, top_left_pixel_location.x_pos + 1, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 2, top_left_pixel_location.x_pos + 1, color);
    led_display__draw_pixel(top_left_pixel_location.y_pos + 3, top_left_pixel_location.x_pos + 1, color);

    led_display__draw_pixel(top_left_pixel_location.y_pos + 2, top_left_pixel_location.x_pos, color);
  }
}