#include "game_structs.h"

location game_structs__add_location_offsets(location start_location, char *offsets, uint8_t size_of_offsets) {
  location new_location = start_location;
  for (int i = 0; i < size_of_offsets; i++) {
    switch (offsets[i]) {
    case 'N':
      new_location.y_pos -= 1;
      break;
    case 'E':
      new_location.x_pos += 1;
      break;
    case 'S':
      new_location.y_pos += 1;
      break;
    case 'W':
      new_location.x_pos -= 1;
      break;
    }
  }

  return new_location;
}
int8_t NORTH_DIRECTION = -1;
int8_t EAST_DIRECTION = 1;
int8_t SOUTH_DIRECTION = 1;
int8_t WEST_DIRECTION = -1;

location game_structs__add_location_offsets_by_row_col(location start_location, int x, int y) {
  location new_location = {0, 0};

  new_location.y_pos = start_location.y_pos + y;

  new_location.x_pos = start_location.x_pos + x;

  return new_location;
}