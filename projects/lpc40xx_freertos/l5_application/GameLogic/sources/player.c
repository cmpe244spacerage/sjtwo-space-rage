#include "player.h"

player_s player__human_init(pixel_color_e player_color, controller_pins *controller, uint8_t player_number) {
  player_s new_player;
  new_player.controller = controller;
  new_player.player_number = player_number;
  new_player.player_details.player_color = player_color;
  new_player.player_details.current_speed = SLOW;
  new_player.player_details.ticks_till_move = SLOW;
  new_player.has_crashed = false;
  return new_player;
}
player_s player__ai_init(pixel_color_e player_color, ai_difficulty_e difficulty, uint8_t player_number) {
  player_s new_ai;
  new_ai.controller = NULL;
  new_ai.player_number = player_number;
  new_ai.player_details.player_color = player_color;
  new_ai.player_details.current_speed = SLOW;
  new_ai.player_details.ticks_till_move = SLOW;
  new_ai.has_crashed = false;
  return new_ai;
}

void player__controller_init(controller_pins *controller, gpio_s up, gpio_s right, gpio_s down, gpio_s left);

bool player__tick(player_s *player) {
  bool player_ready_to_move = false;
  if (player->player_details.ticks_till_move == 0) {
    player_ready_to_move = true;
    player->player_details.ticks_till_move = player->player_details.current_speed;
  }
  player->player_details.ticks_till_move--;
  return player_ready_to_move;
}

void player__update_location(player_s *player) {
  location new_location = player->player_details.current_location;
  switch (player->player_details.new_direction) {
  case NORTH:
    new_location.y_pos = player->player_details.current_location.y_pos - 1;
    break;
  case EAST:
    new_location.x_pos = player->player_details.current_location.x_pos + 1;
    break;
  case SOUTH:
    new_location.y_pos = player->player_details.current_location.y_pos + 1;
    break;
  case WEST:
    new_location.x_pos = player->player_details.current_location.x_pos - 1;
    break;
  }
  player->player_details.current_location = new_location;
}
