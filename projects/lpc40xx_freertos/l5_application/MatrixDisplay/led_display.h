#pragma once
#include "game_structs.h"
#include <stdint.h>

void led_display__init(void);
void led_display__draw_pixel(uint8_t row, uint8_t column, pixel_color_e pixel_color);
pixel_color_e led_display__get_pixel(uint8_t row, uint8_t column);
void led_display__display_driver(void);