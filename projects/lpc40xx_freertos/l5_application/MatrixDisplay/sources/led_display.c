#include "led_display.h"
#include "FreeRTOS.h"
#include "delay.h"
#include "gpio.h"

#define DISPLAY_ROWS 64
#define DISPLAY_COLUMNS 64

static uint8_t display_matrix[64][64] = {0};

typedef struct {
  gpio_s r1, g1, b1;
  gpio_s r2, g2, b2;
  gpio_s latch, output_enable, clock;
  gpio_s A, B, C, D, E;

} led_display_gpio_pins;

static led_display_gpio_pins led_display_pins;

static void led_display__select_row(uint8_t row);
static void led_display__disable_latch(void);
static void led_display__enable_latch(void);
static void led_display__disable_output_enable(void);
static void led_display__enable_output_enable(void);
static void led_display__clock_in_pixel_data(void);
static void led_display__set_pixel(uint8_t row, uint8_t column);

/*******************************************************************************
 *
 *                      P U B L I C    F U N C T I O N S
 *
 ******************************************************************************/
void led_display__init(void) {
  /* Initialize RGB pins for upper and lower half of matrix */
  gpio__function_e gpio_function = GPIO__FUNCITON_0_IO_PIN;
  led_display_pins.r1 = gpio__construct_with_function(0, 6, gpio_function);
  led_display_pins.g1 = gpio__construct_with_function(0, 7, gpio_function);
  led_display_pins.b1 = gpio__construct_with_function(0, 8, gpio_function);

  gpio__set_as_output(led_display_pins.r1);
  gpio__set_as_output(led_display_pins.g1);
  gpio__set_as_output(led_display_pins.b1);

  led_display_pins.r2 = gpio__construct_with_function(0, 26, gpio_function);
  led_display_pins.g2 = gpio__construct_with_function(0, 9, gpio_function);
  led_display_pins.b2 = gpio__construct_with_function(1, 31, gpio_function);

  gpio__set_as_output(led_display_pins.r2);
  gpio__set_as_output(led_display_pins.g2);
  gpio__set_as_output(led_display_pins.b2);

  /* Initialize GPIO pins for latch, output enable, and clock */
  led_display_pins.latch = gpio__construct_with_function(1, 29, gpio_function);
  led_display_pins.output_enable = gpio__construct_with_function(2, 2, gpio_function);
  led_display_pins.clock = gpio__construct_with_function(2, 0, gpio_function);

  gpio__set_as_output(led_display_pins.latch);
  gpio__set_as_output(led_display_pins.output_enable);
  gpio__set_as_output(led_display_pins.clock);

  gpio__set(led_display_pins.output_enable); // < SET OE as it is an active low pin

  /* Initialize pins for row selection */
  led_display_pins.A = gpio__construct_with_function(1, 20, gpio_function);
  led_display_pins.B = gpio__construct_with_function(1, 30, gpio_function);
  led_display_pins.C = gpio__construct_with_function(1, 28, gpio_function);
  led_display_pins.D = gpio__construct_with_function(1, 23, gpio_function);
  led_display_pins.E = gpio__construct_with_function(0, 25, gpio_function);

  gpio__set_as_output(led_display_pins.A);
  gpio__set_as_output(led_display_pins.B);
  gpio__set_as_output(led_display_pins.C);
  gpio__set_as_output(led_display_pins.D);
  gpio__set_as_output(led_display_pins.E);
}
void led_display__draw_pixel(uint8_t row, uint8_t column, pixel_color_e pixel_color) {
  display_matrix[row][column] = pixel_color;
}

pixel_color_e led_display__get_pixel(uint8_t row, uint8_t column) { return display_matrix[row][column]; }

void led_display__display_driver(void) {
  for (uint8_t row = 0; row < 32; row++) {
    for (uint8_t column = 0; column < 64; column++) {
      led_display__set_pixel(row, column);
      led_display__clock_in_pixel_data();
    }

    led_display__enable_latch();

    led_display__select_row(row);
    led_display__disable_latch();
    led_display__enable_output_enable();
    delay__us(100);
    led_display__disable_output_enable();
  }
}

/*******************************************************************************
 *
 *                      E N D  O F  P U B L I C    F U N C T I O N S
 *
 ******************************************************************************/
/*******************************************************************************
 *
 *                      P R I V A T E    F U N C T I O N S
 *
 ******************************************************************************/
static void led_display__select_row(uint8_t row) {
  uint8_t row_select_A_bit = (1 << 0);
  uint8_t row_select_B_bit = (1 << 1);
  uint8_t row_select_C_bit = (1 << 2);
  uint8_t row_select_D_bit = (1 << 3);
  uint8_t row_select_E_bit = (1 << 4);

  (row & row_select_A_bit) ? gpio__set(led_display_pins.A) : gpio__reset(led_display_pins.A);
  (row & row_select_B_bit) ? gpio__set(led_display_pins.B) : gpio__reset(led_display_pins.B);
  (row & row_select_C_bit) ? gpio__set(led_display_pins.C) : gpio__reset(led_display_pins.C);
  (row & row_select_D_bit) ? gpio__set(led_display_pins.D) : gpio__reset(led_display_pins.D);
  (row & row_select_E_bit) ? gpio__set(led_display_pins.E) : gpio__reset(led_display_pins.E);
}

static void led_display__disable_latch(void) { gpio__reset(led_display_pins.latch); }
static void led_display__enable_latch(void) { gpio__set(led_display_pins.latch); }
static void led_display__disable_output_enable(void) { gpio__set(led_display_pins.output_enable); }
static void led_display__enable_output_enable(void) { gpio__reset(led_display_pins.output_enable); }
static void led_display__clock_in_pixel_data(void) {
  gpio__set(led_display_pins.clock);
  gpio__reset(led_display_pins.clock);
}

static void led_display__set_pixel(uint8_t row, uint8_t column) {
  const uint8_t one_to_thirty_scan_rate_matrix_row_offset = 32;
  const uint8_t bottom_half_display = row + one_to_thirty_scan_rate_matrix_row_offset;

  /* bit mapping for R,G,B pixel */
  const uint8_t pixel_red_value_bit = (1 << 2);
  const uint8_t pixel_green_value_bit = (1 << 1);
  const uint8_t pixel_blue_value_bit = (1 << 0);

  (display_matrix[row][column] & pixel_red_value_bit) ? gpio__set(led_display_pins.r1)
                                                      : gpio__reset(led_display_pins.r1);
  (display_matrix[row][column] & pixel_green_value_bit) ? gpio__set(led_display_pins.g1)
                                                        : gpio__reset(led_display_pins.g1);
  (display_matrix[row][column] & pixel_blue_value_bit) ? gpio__set(led_display_pins.b1)
                                                       : gpio__reset(led_display_pins.b1);

  (display_matrix[bottom_half_display][column] & pixel_red_value_bit) ? gpio__set(led_display_pins.r2)
                                                                      : gpio__reset(led_display_pins.r2);
  (display_matrix[bottom_half_display][column] & pixel_green_value_bit) ? gpio__set(led_display_pins.g2)
                                                                        : gpio__reset(led_display_pins.g2);
  (display_matrix[bottom_half_display][column] & pixel_blue_value_bit) ? gpio__set(led_display_pins.b2)
                                                                       : gpio__reset(led_display_pins.b2);
}

/*******************************************************************************
 *
 *               E N D    O F    P R I V A T E    F U N C T I O N S
 *
 ******************************************************************************/