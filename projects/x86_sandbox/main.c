#include <stdio.h>

int main(void) {
  char buf[40] = {};
  printf("Hello world!\n");
  printf("It is \033[31mnot\033[39m intelligent to use \033[32mhardcoded ANSI\033[39m codes!");
  buf[0] = getchar();
  printf("%s", buf);
  return 0;
}
